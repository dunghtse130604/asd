﻿using JB2.Core;
using Microsoft.Azure.Devices.Client;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Data.Common;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ExecutionContext = Microsoft.Azure.WebJobs.ExecutionContext;
using TransportType = Microsoft.Azure.Devices.Client.TransportType;

namespace JB2.Healthcheck
{
    public static class HealthCheckReset
    {
        public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, ILogger logger, CloudBlobContainer healthCheckContainer, ExecutionContext context)
        {
            var deviceId = ExternalConfig.HealthCheckDeviceIdReset;
            using (var deviceClient = DeviceClient.CreateFromConnectionString(HealthCheckUtil.GetDeviceConnectionString(deviceId), TransportType.Mqtt))
            {
                await deviceClient.OpenAsync();
                var sourceIframe = File.ReadAllBytes(context.FunctionDirectory + @"\data\Iframe");

                var time = UnixTime.Now;
                var resetContent = new
                {
                    time,
                    gps = new Gps
                    {
                        valid = 1,
                        latitude = 35.785018,
                        longitude = 139.313097,
                        speed = 40,
                        heading = 0
                    }
                };

                //Send reset message to Gateway
                var resetMessage = HealthCheckUtil.CreateMessage(MessageType.Reset, resetContent, null, sourceIframe);

                await deviceClient.SendEventAsync(resetMessage);

                //Wait reset message reached to SCOM EventHub
                if (!await healthCheckContainer.WaitMessageReached(deviceId, MessageType.Reset, time))
                {
                    logger.Error("Timeout due to reset message not reached");
                    return new HttpResponseMessage(HttpStatusCode.RequestTimeout);
                }
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
        }
    }
}