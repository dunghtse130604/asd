﻿using JB2.Core;
using Microsoft.Azure.Devices.Client;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.IO;
using System.Threading.Tasks;

namespace JB2.Healthcheck
{
    public static class HealthCheckUtil
    {
        public static string GetDeviceConnectionString(string deviceId)
        {
            return AppEnvironment.DeviceConnectionString.Replace(Constants.DefaultDeviceId, deviceId);
        }

        public static string GetRestApi(string deviceId)
        {
            return AppEnvironment.RestApiURL.Replace(Constants.DefaultDeviceId, deviceId);
        }

        public static Message CreateMessage(string messageType, object content, byte[] dataBytes = null, byte[] mediaBytes = null)
        {
            var jsonBytes = content.ToJsonBytes();
            using (var bodyStream = new MemoryStream())
            using (var bodyWriter = new BinaryWriter(bodyStream))
            {
                bodyWriter.BaseStream.Position = 0;
                bodyWriter.Write(jsonBytes);
                if (dataBytes != null)
                {
                    bodyWriter.Write(dataBytes);
                }
                if (mediaBytes != null)
                {
                    bodyWriter.Write(mediaBytes);
                }

                var d2cMsg = new Message(bodyStream.ToArray())
                {
                    Properties =
                    {
                        [EventDataProps.SdId] = Constants.HealthcheckSdId,
                        [EventDataProps.MessageType] = messageType,
                        [EventDataProps.SentTime] = UnixTime.Now.ToString(),
                        [EventDataProps.ContentSize] = jsonBytes.Length.ToString()
                    }
                };
                if (dataBytes != null)
                {
                    d2cMsg.Properties[EventDataProps.DataSize] = dataBytes.Length.ToString();
                }
                if (mediaBytes != null)
                {
                    d2cMsg.Properties[EventDataProps.MediaSize] = mediaBytes.Length.ToString();
                }

                return d2cMsg;
            }
        }

        public static async Task<bool> WaitMessageReached(this CloudBlobContainer healthCheckContainer,
            string deviceId, string messageType, long dataTime, int timeout = 90)
        {
            var blob = healthCheckContainer.GetBlockBlobReference(
                $"{messageType}_{deviceId}{dataTime.ToDatetimeString(TransmissionData.DateFormat)}");
            for (; timeout > 0; timeout--)
            {
                await Task.Delay(TimeSpan.FromSeconds(1));
                if (await blob.ExistsAsync())
                {
                    //Clear blob after checking
                    await blob.DeleteAsync();
                    return true;
                }
            }
            return false;
        }
    }
}