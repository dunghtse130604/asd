﻿namespace JB2.Healthcheck
{
    public class Constants
    {
        public const string HealthcheckSdId = "HEALTH_CHECK_SDID";
        public const string DefaultDeviceId = "{deviceId}";
    }
}