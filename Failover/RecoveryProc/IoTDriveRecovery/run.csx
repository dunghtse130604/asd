﻿#r "Microsoft.ServiceBus"
#r "Newtonsoft.Json"
#r "Microsoft.Azure.WebJobs"
using System;
using System.Text;
using Microsoft.Azure.WebJobs;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;

public static async Task Run(EventData iotDriveMessagePoison, IAsyncCollector<EventData> outputRetryMessage, TraceWriter log)
{
    var content = $"Properties={JsonConvert.SerializeObject(iotDriveMessagePoison.Properties)}, EnqueuedTimeUtc={iotDriveMessagePoison.EnqueuedTimeUtc}";
    log.Info($"[DriveRecovery] {content}");

    await outputRetryMessage.AddAsync(iotDriveMessagePoison);
    await outputRetryMessage.FlushAsync();
}