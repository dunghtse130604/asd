﻿#r "Microsoft.ServiceBus"
#r "Newtonsoft.Json"
#r "Microsoft.Azure.WebJobs"
using System;
using System.Text;
using Microsoft.Azure.WebJobs;
using Microsoft.ServiceBus.Messaging;
using Newtonsoft.Json;

public static async Task Run(EventData transmissionMessagePoison, IAsyncCollector<EventData> outputRetryMessage, TraceWriter log)
{
    var content = $"Properties={JsonConvert.SerializeObject(transmissionMessagePoison.Properties)}, EnqueuedTimeUtc={transmissionMessagePoison.EnqueuedTimeUtc}";
    log.Info($"[TransmissionRecovery] {content}");

    await outputRetryMessage.AddAsync(transmissionMessagePoison);
    await outputRetryMessage.FlushAsync();
}