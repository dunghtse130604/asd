﻿using System.Web.Mvc;
using System.Web.Routing;

namespace JB2.BOM
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "DeviceReport",
                url: "device-report",
                defaults: new { controller = "DeviceReport", action = "Index" }
            );

            routes.MapRoute(
                name: "TelemetryReport",
                url: "telemetry-report",
                defaults: new { controller = "TelemetryReport", action = "Index" }
            );

            routes.MapRoute(
                name: "TripReport",
                url: "trip-report",
                defaults: new { controller = "TripReport", action = "Index" }
            );

            routes.MapRoute(
                name: "GetUsageInfo",
                url: "usageinfo",
                defaults: new { controller = "Home", action = "GetUsageInfo" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Default", id = UrlParameter.Optional }
            );
        }
    }
}
