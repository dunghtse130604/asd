﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using Microsoft.PowerBI.Api.V2;
using Microsoft.PowerBI.Api.V2.Models;
using Microsoft.Rest;
using System;
using System.Threading.Tasks;
using Microsoft.ApplicationInsights;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Diagnostics;
using System.Linq;
using Microsoft.Azure.Documents;

namespace JB2.BOM
{
    public class BomCore
    {
        const string AuthorityUrl = "https://login.windows.net/common/oauth2/authorize";
        const string ResourceUrl = "https://analysis.windows.net/powerbi/api";
        const string ApiUrl = "https://api.powerbi.com/";

        private static readonly int RequestCacheExpiredTime = 600;
        private static readonly int RequestTimeout = 60;

        public static async Task<TokenModel> GetToken(string repordId)
        {
            var tokenCredentials = await GetTokenCredentials();

            // Create a Power BI Client object. It will be used to call Power BI APIs.
            using (var client = new PowerBIClient(new Uri(ApiUrl), tokenCredentials))
            {
                var generateTokenRequestParameters = new GenerateTokenRequest(accessLevel: "view");
                var tokenResponse = await client.Reports.GenerateTokenInGroupAsync(BomEnvironment.EmbedGroupId, repordId,
                    generateTokenRequestParameters);

                // Generate Embed Configuration.
                var embedToken = $"https://app.powerbi.com/reportEmbed?reportId={repordId}&groupId={BomEnvironment.EmbedGroupId}";
                return TokenModel.Ok(tokenResponse.Token, embedToken, repordId);
            }
        }

        public static async Task<int> MigrateData(string deviceId, string queryString, string queryId = null)
        {
            var sw = Stopwatch.StartNew();
            var ai = new TelemetryClient();
            ai.Info($"Request: deviceId = {deviceId}, query = {queryString}");
            var stringRequest = $"{deviceId}_{queryId}";

            try
            {
                int dataCount = 0;
                using (var context = new BomDbContext(BomEnvironment.SqlServerConnsStr))
                {
                    // Check request chache entity
                    var over10Min = DateTime.UtcNow.AddSeconds(-RequestCacheExpiredTime);
                    QueryCache entity = await (from r in context.QueryCaches
                                               where r.query == stringRequest && r.updatedDate >= over10Min
                                               select r).FirstOrDefaultAsync();
                    if (entity != null)
                    {
                        // If exists, wait the processing done if the request is in processing
                        ai.Info($"Request: {stringRequest} is cached.");
                        var time = Stopwatch.StartNew();
                        while (entity.status == 0)
                        {
                            await Task.Delay(TimeSpan.FromSeconds(3));
                            context.Entry(entity).Reload();
                            if (time.Elapsed > TimeSpan.FromSeconds(RequestTimeout))
                            {
                                throw new TimeoutException("Request timeout");
                            }
                        }
                        dataCount = entity.dataCount;
                    }
                    else
                    {
                        ai.Info($"Request: {stringRequest} is not cached.");
                        // Create request cache entity wiht status 0 (in processing)
                        entity = new QueryCache
                        {
                            query = stringRequest,
                            status = 0,
                            updatedDate = DateTime.UtcNow
                        };
                        context.QueryCaches.AddOrUpdate(entity);
                        await context.SaveChangesAsync();

                        // Migration from CosmosDB to SQL DB
                        try
                        {
                            var dataTelemetry = await DocumentDBRepo.GetItemsAsync(deviceId, queryString, ai);
                            dataCount = dataTelemetry.Count;
                            if (dataCount > 0)
                            {
                                await context.Insert(dataTelemetry);
                            }
                        }
                        catch (DocumentClientException de)
                        {
                            if (429 == (int)de.StatusCode)
                            {
                                return -1;
                            }
                        }
                        catch (Exception ex)
                        {
                            ai.Error(stringRequest, ex);
                            // Remove the request cache entity
                            context.QueryCaches.Remove(entity);
                            await context.SaveChangesAsync();
                            throw;
                        }

                        // Update the request cache entity
                        entity.status = 1;
                        entity.dataCount = dataCount;
                        context.QueryCaches.AddOrUpdate(entity);
                        await context.SaveChangesAsync();
                    }
                }
                return dataCount;
            }
            catch (Exception ex)
            {
                ai.Error(stringRequest, ex);
                throw;
            }
            finally
            {
                sw.Stop();
                ai.TrackMetric("[Complete request]", sw.ElapsedMilliseconds / 1000.0);
            }
        }

        public static async Task<int?> GetUsageInfo()
        {
            var tokenCredentials = await GetTokenCredentials();

            using (var client = new PowerBIClient(new Uri(ApiUrl), tokenCredentials))
            {
                var usageInfo = client.AvailableFeatures.GetAvailableFeatureByName("embedTrial")
                                      .AdditionalInfo.Usage;
                return usageInfo;
            }
        }

        private static async Task<TokenCredentials> GetTokenCredentials()
        {
            var credential = new UserPasswordCredential(BomEnvironment.EmbedUsername, BomEnvironment.EmbedPassword);
            var authenticationContext = new AuthenticationContext(AuthorityUrl);
            var authenticationResult = await authenticationContext.AcquireTokenAsync(ResourceUrl, BomEnvironment.EmbedClientId, credential);
            return new TokenCredentials(authenticationResult.AccessToken, "Bearer");
        }
    }
}