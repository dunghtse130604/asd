﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace JB2.BOM
{
    public static class TelemetryCache
    {
        public const string TableName = "TelemetryCache";
        public const string TempTableName = "#TelemetryCache";

        public class Model
        {
            public string Name;
            public string Type;
            public string DocNode;
            public bool Index;
        }

        public static Model[] Keys =
        {
            new Model { Name = "deviceId", Type = "varchar(15)", DocNode = "deviceId" },
            new Model { Name = "id", Type = "varchar(35)", DocNode = "id" },
        };

        public static Model[] Fields = {
            new Model { Name = "sdId", Type = "varchar(15)", DocNode = "sdId", Index = true },
            new Model { Name = "messageType", Type = "varchar(10)", DocNode = "messageType", Index = true },
            new Model { Name = "receivedTime", Type = "datetime", DocNode = "receivedTime", Index = true },
            new Model { Name = "time", Type = "datetime", DocNode = "data.time", Index = true },
            new Model { Name = "engineOnTime", Type = "datetime", DocNode = "data.engineOnTime", Index = true },
            new Model { Name = "gpsTime", Type = "datetime", DocNode = "data.lastGps.time" },
            new Model { Name = "gpsValid", Type = "int", DocNode = "data.gps.valid" },
            new Model { Name = "latitude", Type = "float", DocNode = "data.gps.latitude" },
            new Model { Name = "longitude", Type = "float", DocNode = "data.gps.longitude" },
            new Model { Name = "speed", Type = "int", DocNode = "data.gps.speed" },
            new Model { Name = "heading", Type = "int", DocNode = "data.gps.heading" },
            new Model { Name = "duration", Type = "int", DocNode = "data.duration" },
            new Model { Name = "fw", Type = "varchar(15)", DocNode = "data.fw" },
            new Model { Name = "phone", Type = "varchar(15)", DocNode = "data.phone" },
            new Model { Name = "calibration", Type = "bit", DocNode = "data.sensor.calibration" },
            new Model { Name = "angle", Type = "int", DocNode = "data.sensor.angle" },
            new Model { Name = "button", Type = "bit", DocNode = "data.flags.button" },
            new Model { Name = "shock", Type = "bit", DocNode = "data.flags.shock" },
            new Model { Name = "strength", Type = "int", DocNode = "data.strength" },
            new Model { Name = "sdError", Type = "int", DocNode = "data.status" },
            new Model { Name = "logType", Type = "int", DocNode = "data.type", Index = true },
            new Model { Name = "logMessage", Type = "varchar(100)", DocNode = "data.message" },
            new Model { Name = "vpcStatus", Type = "int", DocNode = "data.result.status" },
            new Model { Name = "vpcX", Type = "int", DocNode = "data.result.hd_vp_x" },
            new Model { Name = "vpcY", Type = "int", DocNode = "data.result.hd_vp_y" },
            new Model { Name = "vpcBe", Type = "int", DocNode = "data.result.hd_be_p" },
            new Model { Name = "photo", Type = "varchar(100)", DocNode = "data.photo" },
            new Model { Name = "dateId", Type = "varchar(10)", DocNode = null },
        };

        public static Model[] DuplicatedFields = {
            new Model { Name = "latitude", Type = "float", DocNode = "data.lastGps.latitude" },
            new Model { Name = "longitude", Type = "float", DocNode = "data.lastGps.longitude" },
            new Model { Name = "speed", Type = "int", DocNode = "data.lastGps.speed" },
            new Model { Name = "heading", Type = "int", DocNode = "data.lastGps.heading" },
        };

        public static Model[] AllFields = Keys.Concat(Fields).ToArray();
        public static Model[] MigratedFields = AllFields.Concat(DuplicatedFields).Where(d => d.DocNode != null).ToArray();

        public static string JoinedFormat (this IEnumerable<Model> models, Func<Model, string> format, string separator)
        {
            return string.Join(separator, models.Select(format));
        }

        public static string CommandCreateTable = $@"
            DROP TABLE IF EXISTS {TableName};
            CREATE TABLE {TableName} (
                {Keys.JoinedFormat(d => $"{d.Name} {d.Type} NOT NULL", ", ")},
                {Fields.JoinedFormat(d => $"{d.Name} {d.Type} NULL", ", ")}
                CONSTRAINT PK_{TableName} PRIMARY KEY(
                    {Keys.JoinedFormat(d => $"{d.Name}", ", ")}
                )
            );
            CREATE INDEX {TableName}_Index ON {TableName} (
                {Fields.Where(d => d.Index).JoinedFormat(d => $"{d.Name}", ", ")}
            );";

        public static string CommandCreateTempTable =
            $"SELECT * INTO {TempTableName} FROM {TableName} WHERE deviceId=''";
        public static string CommandCreateAdapter =
            $"SELECT * FROM {TableName} WHERE deviceId=''";
        public static string CommandMerge = $@"
            MERGE INTO {TableName} AS Target
            USING(SELECT * FROM {TempTableName}) AS Source
            ON ({Keys.JoinedFormat(d => $"Target.{d.Name} = Source.{d.Name}", " AND ")})
            WHEN MATCHED THEN
                UPDATE SET {Fields.JoinedFormat(d => $"Target.{d.Name} = Source.{d.Name}", ", ")}
                WHEN NOT MATCHED THEN
                    INSERT ({AllFields.JoinedFormat(d => $"{d.Name}", ", ")})
                    VALUES ({AllFields.JoinedFormat(d => $"Source.{d.Name}", ", ")});";

    }
}