﻿using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace JB2.BOM
{
    public static class BomLogExtension
    {
        public static void Info(this TelemetryClient client, string message,
            [CallerFilePath] string fullPath = "", [CallerLineNumber] int lineNumber = 0)
        {
            var fileName = Path.GetFileName(fullPath);
            client.TrackTrace($"[{fileName}-{lineNumber}] {message}", SeverityLevel.Information);
        }

        public static void Debug(this TelemetryClient client, string message,
            [CallerFilePath] string fullPath = "", [CallerLineNumber] int lineNumber = 0)
        {
            var fileName = Path.GetFileName(fullPath);
            client.TrackTrace($"[{fileName}-{lineNumber}] {message}", SeverityLevel.Verbose);
        }

        public static void Warning(this TelemetryClient client, string message,
            [CallerFilePath] string fullPath = "", [CallerLineNumber] int lineNumber = 0)
        {
            var fileName = Path.GetFileName(fullPath);
            client.TrackTrace($"[{fileName}-{lineNumber}] {message}", SeverityLevel.Warning);
        }

        public static void Error(this TelemetryClient client, string message, Exception ex,
            [CallerFilePath] string fullPath = "", [CallerLineNumber] int lineNumber = 0)
        {
            var fileName = Path.GetFileName(fullPath);
            client.TrackTrace($"[{fileName}-{lineNumber}] {message}", SeverityLevel.Error);
            client.TrackException(ex);
        }
    }
}