﻿using Microsoft.ApplicationInsights;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace JB2.BOM
{
    public class DeviceReportController : HomeController
    {
        [HttpGet]
        public async Task<ActionResult> Index()
        {
            if (!Request.IsAuthenticated)
            {
                return Redirect(SignInPageUrl);
            }
            var ai = new TelemetryClient();
            ai.Info($"{HttpContext.User.Identity.Name} request view page Device Management");

            return View(await BomCore.GetToken(BomEnvironment.EmbedReportIdDevice));
        }
    }
}