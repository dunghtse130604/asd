﻿using System;
using System.Globalization;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace JB2.BOM
{
    public class TripReportController : HomeController
    {
        [HttpGet]
        public async Task<ActionResult> Index(string deviceId, string date)
        {
            if (!Request.IsAuthenticated)
            {
                return Redirect(SignInPageUrl);
            }

            if (System.Web.HttpContext.Current.Request.Url.Query == "")
            {
                ViewBag.deviceId = string.Empty;
                ViewBag.date = DateTimeOffset.UtcNow.AddHours(9).ToString("yyyy-MM-dd");
                return View(TokenModel.Ok());
            }
            if (String.IsNullOrEmpty(deviceId) || String.IsNullOrEmpty(date))
            {
                return View(TokenModel.Error(Resources.Global.th_notification_bad_request));
            }

            ViewBag.deviceId = deviceId;
            ViewBag.date = date;

            var fromTime = DateTimeOffset.ParseExact($"{date}+09:00", "yyyy-MM-ddzzz", CultureInfo.CurrentCulture).ToUnixTimeSeconds();
            string queryString = $@"SELECT * FROM c WHERE c.data.time >= {fromTime} AND c.data.time < {fromTime + 86400} AND c.messageType in ('drive')";
            var count = await BomCore.MigrateData(deviceId, queryString, date);
            if (count > 0)
            {
                return View(await BomCore.GetToken(BomEnvironment.EmbedReportIdTrip));
            }
            if (count == 0)
            {
                return View(TokenModel.Error(Resources.Global.th_notification_no_data));
            }
            return View(TokenModel.Error(Resources.Global.server_busy));

        }
    }
}