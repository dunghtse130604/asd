﻿namespace JB2.BOM
{
    public class TokenModel
    {
        public string EmbedToken { get; set; }

        public string EmbedUrl { get; set; }

        public string ReportId { get; set; }

        public string ErrorMessage { get; set; }

        public TokenModel() { }

        public static TokenModel Ok()
        {
            return new TokenModel();
        }
        public static TokenModel Ok(string embedToken, string embedUrl, string reportId)
        {
            return new TokenModel
            {
                EmbedToken = embedToken,
                EmbedUrl = embedUrl,
                ReportId = reportId
            };
        }

        public static TokenModel Error(string message)
        {
            return new TokenModel { ErrorMessage = message };
        }
    }
}