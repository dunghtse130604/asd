﻿using CommandLine;

namespace CreateDocumentDB
{
    public class Options
    {
        [Option('e', "EndpointUrl", Required = true,
            HelpText = "Input EndpointUrl")]
        public string EndpointUrl { get; set; }

        [Option('p', "PrimaryKey", Required = true,
            HelpText = "Input PrimaryKey")]
        public string PrimaryKey { get; set; }

        [Option('d', "DbName", Required = true,
            HelpText = "Input DbName")]
        public string DbName { get; set; }
    }
}
