﻿using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

namespace CreateDocumentDB
{
    public class DocumentDbUtils
    {
        public async Task CreateDatabase(DocumentClient client, string databaseName)
        {
            await client.CreateDatabaseIfNotExistsAsync(new Database { Id = databaseName });
        }

        // Create Collection with PartitionKey and Index
        public async Task CreateIndexParCollection(DocumentClient client, string databaseName, string collectionId, string partitionKey, List<string> pathArray, int throughput)
        {

            DocumentCollection myCollection = new DocumentCollection();
            myCollection.Id = collectionId;
            myCollection.PartitionKey.Paths.Add(partitionKey);

            myCollection.IndexingPolicy.ExcludedPaths.Add(new ExcludedPath { Path = "/" });

            foreach (string pathString in pathArray)
            {
                myCollection.IndexingPolicy.IncludedPaths.Add(
                    new IncludedPath
                    {
                        Path = pathString,
                        Indexes = new Collection<Index> { new HashIndex(DataType.String) { Precision = 8 },
                            new RangeIndex(DataType.Number) { Precision = 8 }}
                    });
            }

            //default index rule
            myCollection.IndexingPolicy.IndexingMode = IndexingMode.Consistent;

            await client.CreateDocumentCollectionAsync(UriFactory.CreateDatabaseUri(databaseName),
                myCollection, new RequestOptions { OfferThroughput = throughput });
        }

    }
}
