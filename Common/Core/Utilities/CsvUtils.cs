﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JB2.Core
{
    public abstract class BaseCsv
    {
        public string[] Cells { get; set; }

        public void ReserveCells(int count)
        {
            Cells = new string[count];
        }

        public void ParseCsvLine(string line)
        {
            Cells = line.Split('\t');
        }

        public override string ToString()
        {
            return string.Join("\t", Cells);
        }

        public virtual string Header { get { return ""; } }
    }

    public class CsvUtils
    {
        public static IEnumerable<T> ParseCsvString<T>(string csv) where T : BaseCsv, new()
        {
            var list = new List<T>();
            csv.Trim()      // TCU sends csv including linefeed at end of the data
                .Split('\n')
                .Skip(1)    // Remove header
                .ToList()
                .ForEach(line =>
                {
                    var item = new T();
                    item.ParseCsvLine(line);
                    list.Add(item);
                });
            return list;
        }

        public static byte[] ConvertCsvBytes<T>(IList<T> list) where T : BaseCsv
        {
            var header = list.FirstOrDefault()?.Header;
            return Encoding.UTF8.GetBytes(header + string.Join("\n", list));
        }

    }
}
