﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace JB2.Core
{
    public static class LogMessageFactory
    {
        public static Log Create(string jsonSrc)
        {
            var src = JsonConvert.DeserializeObject<LogSource>(jsonSrc);
            var l = new Log
            {
                index = src.Index,
                error = src.Type >= 0 ? 0 : 1,
                time = src.Time,
                type = src.Type
            };
            LogDataType ldt;
            switch (src.Type)
            {
                case 1:
                    ldt = new LogDataType { DataType = "BOOT", Prefix = "BOOT", Detail = "Boot on" };
                    break;
                case 4:
                    ldt = new LogDataType { DataType = "RECORDON", Prefix = "RECORDON", Detail = "Record on" };
                    break;
                case 5:
                    ldt = new LogDataType { DataType = "RECORDOFF", Prefix = "RECORDOFF", Detail = "Record off" };
                    break;
                case 6:
                    ldt = new LogDataType { DataType = "FW_UPGRADE", LogStructure = new FwUpgradeLogStructure(src), Prefix = "FW_UPGRADE" };
                    break;
                case 7:
                    ldt = new LogDataType { DataType = "CONFIG_UP", LogStructure = new ConfigUpLogStructure(src), Prefix = "CONFIG_UP", Detail = "configuration file type" };
                    break;
                case 10:
                    ldt = new LogDataType { DataType = "GPS_TIME_SYNC", LogStructure = new GpsTimeSyncLogStructure(src), Prefix = "GPS_TIME_SYNC" };
                    break;
                case 15:
                    ldt = new LogDataType { DataType = "SD_FORMAT", Prefix = "SD_FORMAT", Detail = "SD Format" };
                    break;
                case 16:
                    ldt = new LogDataType { DataType = "TIME_SET", LogStructure = new TimeSetLogStructure(src), Prefix = "TIME_SET" };
                    break;
                case 17:
                    ldt = new LogDataType { DataType = "WATCHDOG", LogStructure = new WatchdogLogStructure(src), Prefix = "WATCHDOG" };
                    break;
                case 19:
                    ldt = new LogDataType { DataType = "DEFAULT_CONF", Prefix = "DEFAULT_CONF", Detail = "System run with default config" };
                    break;
                case 33:
                    ldt = new LogDataType { DataType = "IMEI_UP", LogStructure = new ImeiUpLogStructure(src), Prefix = "IMEI_UP", Detail = "IMEI" };
                    break;
                case 34:
                    ldt = new LogDataType { DataType = "TEMP_PW_CTRL", LogStructure = new TempPwCtrlLogStructure(src), Prefix = "TEMP_PW_CTRL" };
                    break;
                case 37:
                    ldt = new LogDataType { DataType = "SYSTEM_DRID", LogStructure = new SystemDridLogStructure(src), Prefix = "SYSTEM_DRID" };
                    break;
                case 38:
                    ldt = new LogDataType { DataType = "FW_VERSION", LogStructure = new FwVersionLogStructure(src), Prefix = "FW_VERSION", Detail = "FW" };
                    break;
                case 59:
                    ldt = new LogDataType { DataType = "GPS_FW_UPGRADE", LogStructure = new GpsFwUpgradeLogStructure(src), Prefix = "GPS_FW_UPGRADE" };
                    break;
                case 60:
                    ldt = new LogDataType { DataType = "GPS_FW_VERSION", LogStructure = new FwVersionLogStructure(src), Prefix = "GPS", Detail = "FW" };
                    break;
                case -5:
                    ldt = new LogDataType { DataType = "ERROR_SD", LogStructure = new ErrorSdLogStructure(src), Prefix = "(0x0005)SD Error" };
                    break;
                case -6:
                    ldt = new LogDataType { DataType = "ERROR_DEVICE", LogStructure = new ErrorDeviceLogStructure(src), Prefix = "(0x0006)Device Error" };
                    break;
                case -7:
                    ldt = new LogDataType { DataType = "ERROR_NETWORK", LogStructure = new ErrorNetworkLogStructure(src), Prefix = "(0x0007)Network Error" };
                    break;
                case -9:
                    ldt = new LogDataType { DataType = "ERROR_SYSTEM", LogStructure = new ErrorSystemLogStructure(src), Prefix = "(0x0009)System Error" };
                    break;
                default:
                    return null;
            }

            l.message = ldt.ToString().Trim();
            return l;
        }
    }

    public class LogSource
    {
        [JsonProperty("time")]
        public long Time { get; set; }

        [JsonProperty("index")]
        public int Index { get; set; }

        [JsonProperty("type")]
        public int Type { get; set; }

        //Optional properties
        [JsonProperty("drid")]
        public string DriverId { get; set; }

        [JsonProperty("fw_version")]
        public string FwVersion { get; set; }

        [JsonProperty("imei")]
        public string Imei { get; set; }

        [JsonProperty("result")]
        public int Result { get; set; }

        [JsonProperty("old_version")]
        public string OldVersion { get; set; }

        [JsonProperty("new_version")]
        public string NewVersion { get; set; }

        [JsonProperty("source")]
        public int Source { get; set; }

        [JsonProperty("destination")]
        public int Destination { get; set; }

        [JsonProperty("file_type")]
        public int FileType { get; set; }

        [JsonProperty("old_time")]
        public long OldTime { get; set; }

        [JsonProperty("new_time")]
        public long NewTime { get; set; }

        [JsonProperty("data")]
        public int Data { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("error")]
        public int Error { get; set; }

        [JsonProperty("temp_3g")]
        public int Temp3G { get; set; }

        [JsonProperty("temp_sensor")]
        public int TempSensor { get; set; }

        [JsonProperty("new_state")]
        public int NewState { get; set; }

        [JsonProperty("old_state")]
        public int OldState { get; set; }

        [JsonProperty("try_count")]
        public int TryCount { get; set; }

        [JsonProperty("error_data1")]
        public int ErrorData1 { get; set; }

        [JsonProperty("error_data2")]
        public int ErrorData2 { get; set; }

        [JsonProperty("error_data3")]
        public int ErrorData3 { get; set; }

        [JsonProperty("error_data4")]
        public int ErrorData4 { get; set; }
    }

    public class LogDataType
    {
        public string DataType { get; set; }
        public string Prefix { get; set; }
        public string Detail { get; set; } = "";
        public BaseLogStructure LogStructure { get; set; }

        public override string ToString()
        {
            return $"{Prefix} {(!string.IsNullOrEmpty(Detail) ? $"{Detail} " : "")}" + LogStructure?.AdditionalInfo();
        }
    }

    public abstract class BaseLogStructure
    {
        public const string LogDateFormat = "yyyyMMdd_HHmmss";

        public const string Undefined = "Undefined";

        public LogSource Src;

        protected BaseLogStructure(LogSource src)
        {
            Src = src;
        }

        public abstract string AdditionalInfo();

        protected string GetMsg(Dictionary<int, string> dic, int key)
        {
            return dic.ContainsKey(key) ? dic[key] : $"({key}){Undefined}";
        }
    }

    public class FwUpgradeLogStructure : BaseLogStructure
    {
        public static readonly Dictionary<int, string> FwResultDic = new Dictionary<int, string>
        {
            {0x01, "upgrade start"},
            {0x02, "upgrade end"},
            {0x03, "upgrade start, upgrade end"},
            {0xfc, "error, fw name is invalid"},
            {0xf8, "error, fw name is invalid"},
            {0xf4, "error, fw-file is not exist"},
            {0xf0 , "error, fw name is invalid"},
            {0xec , "error, create fail"},
            {0xe8 , "error, move fail"},
            {0xe4 , "error, CRC error"},
        };

        public FwUpgradeLogStructure(LogSource src) : base(src) { }

        public override string AdditionalInfo()
        {
            return $"{GetMsg(FwResultDic, Src.Result)} (upgrade start->end: {Src.OldVersion} -> {Src.NewVersion})";
        }
    }

    public class ConfigUpLogStructure : BaseLogStructure
    {
        public static readonly Dictionary<int, string> FileTypeDic = new Dictionary<int, string>
        {
            {0x00, "update setting"},
            {0x01, "update vehicle id"},
            {0x02, "update driver id"}
        };

        public static readonly Dictionary<int, string> SourceDic = new Dictionary<int, string>
        {
            {0x00, "SD card"},
            {0x01, "NAND"},
            {0x02, "HDD"},
            {0x03, "USB"},
            {0x04, "3g network"},
        };

        public static readonly Dictionary<int, string> DestinationDic = new Dictionary<int, string>
        {
            {0x00, "SD card"},
            {0x01, "HDD"},
            {0x02, "NAND"}
        };

        public static readonly Dictionary<int, string> ResultDic = new Dictionary<int, string>
        {
            {0x00, "update fail"},
            {0x01, "update success"},
            {0x02, "update skip, cause of same to old file"}
        };

        public ConfigUpLogStructure(LogSource src) : base(src) { }

        public override string AdditionalInfo()
        {
            return $"({GetMsg(FileTypeDic, Src.FileType)}) - source position ({GetMsg(SourceDic, Src.Source)}) - apply position ({GetMsg(DestinationDic, Src.Destination)}) - result ({GetMsg(ResultDic, Src.Result)})";
        }
    }

    public class GpsTimeSyncLogStructure : BaseLogStructure
    {
        public GpsTimeSyncLogStructure(LogSource src) : base(src) { }

        public override string AdditionalInfo()
        {
            //TODO: Fix missing 'code' of Error Code: $"Error Code: {Src.Result}" later
            var resultText = Src.Result == 0 ? "OK" : "Error Code:";
            return $"{resultText} - Old Time: {Src.OldTime.ToJstDateTimeString(LogDateFormat)} - New Time: {Src.NewTime.ToJstDateTimeString(LogDateFormat)}";
        }
    }

    public class TimeSetLogStructure : BaseLogStructure
    {
        public TimeSetLogStructure(LogSource src) : base(src) { }

        public override string AdditionalInfo()
        {
            return $"Old Time: {Src.OldTime.ToJstDateTimeString(LogDateFormat)} - New Time: {Src.NewTime.ToJstDateTimeString(LogDateFormat)}";
        }
    }

    public class WatchdogLogStructure : BaseLogStructure
    {
        public static readonly Dictionary<int, string> WatchDogStatusDic = new Dictionary<int, string>
        {
            {0x2000 , "video frame loss" },
            {0x3000 , "audio frame loss" },
            {0x6000 , "thread hang" }
        };

        public WatchdogLogStructure(LogSource src) : base(src) { }

        public override string AdditionalInfo()
        {
            return $"{GetMsg(WatchDogStatusDic, Src.Status)} - {GetMsg(WatchDogStatusDic, Src.Status)} {Src.Data}";
        }
    }

    public class ImeiUpLogStructure : BaseLogStructure
    {
        public static readonly Dictionary<int, string> ResultDic = new Dictionary<int, string>()
        {
            { 0x0, "NG"},
            { 0x1, "OK" }
        };

        public ImeiUpLogStructure(LogSource src) : base(src) { }

        public override string AdditionalInfo()
        {
            return $"update {GetMsg(ResultDic, Src.Result)} - {Src.Imei}";
        }
    }

    public class TempPwCtrlLogStructure : BaseLogStructure
    {
        public static readonly Dictionary<int, string> PowerStateDic = new Dictionary<int, string>
        {
            { 0x00, "3g-on state" },
            { 0x01, "3g-off state" },
            { 0x02, "poweroff state"}
        };

        public static readonly Dictionary<int, string> ErrorDic = new Dictionary<int, string>
        {
            { 0x00, "none" },
            { 0x01, "device(adc) open fail" },
        };

        public TempPwCtrlLogStructure(LogSource src) : base(src) { }

        public override string AdditionalInfo()
        {
            return $"Old state: {GetMsg(PowerStateDic, Src.OldState)} - New state: {GetMsg(PowerStateDic, Src.NewState)} - Temp Sensor: {Src.TempSensor} - Error Code: {GetMsg(ErrorDic, Src.Error)}";
        }
    }

    public class SystemDridLogStructure : BaseLogStructure
    {
        public SystemDridLogStructure(LogSource src) : base(src) { }

        public override string AdditionalInfo()
        {
            return $"DRID: {Src.DriverId}";
        }
    }

    public class FwVersionLogStructure : BaseLogStructure
    {
        public FwVersionLogStructure(LogSource src) : base(src) { }

        public override string AdditionalInfo()
        {
            return $"Version: {Src.FwVersion}";
        }
    }

    public class GpsFwUpgradeLogStructure : BaseLogStructure
    {
        public static readonly Dictionary<int, string> ErrorDic = new Dictionary<int, string>
        {
            { 0x00, "Update Completed"},
            { 0x01, "Timeout: BOOT_ROM_START_CMD"},
            { 0x02, "Timeout: BOOT_ROM_WRITE_CMD (Prefix:0xA1)"},
            { 0x03, "Error: DA Data Write"},
            { 0x04, "Error: BOOT_ROM_JUMP_AMD"},
            { 0x05, "Fail: Get DA Info Report"},
            { 0x06, "Fail: Send DA Mem CMD"},
            { 0x07, "Fail: DA Write BIN(ROM) Data"},
            { 0x08, "Fail: Send BIN(ROM) Packets"},
            { 0x09, "Fail: Send DA Finish CMD"},
            { 0x0A, "Fail: Open Port (for Read Version)"},
            { 0x0B, "Fail: Read Version"},
            { 0x0C, "Fail: File Size Zero"},
            { 0x0D, "Fail: Open Port (for Upgrade)"},
        };

        public readonly Dictionary<int, string> ResultDic = new Dictionary<int, string>
        {
            { 0x01, "Start"},
            { 0x02, "End"},
            { 0x03, "OK"}
        };

        public GpsFwUpgradeLogStructure(LogSource src) : base(src) { }

        public override string AdditionalInfo()
        {
            return $"NewVer:{Src.NewVersion}, Upgrade:{GetMsg(ResultDic, Src.Result)}, {GetMsg(ErrorDic, Src.Error)}. TryCount:{Src.TryCount}";
        }
    }

    public class ErrorSdLogStructure : BaseLogStructure
    {
        public ErrorSdLogStructure(LogSource src) : base(src) { }

        public static readonly Dictionary<int, string> ErrorData1Dic = new Dictionary<int, string>
        {
            {0x01, "(0x01)SD card error"},
            {0x02, "(0x02)SD param error"}
        };

        public static readonly Dictionary<int, string> ErrorData2Dic = new Dictionary<int, string>
        {
            { 0x01, "(0x01)SD Lock"},
            { 0x02, "(0x02)SD read fail"},
            { 0x03, "(0x03)SD not insert" },
            { 0x04, "(0x04)SD write fail" },
            { 0x05, "(0x05)SD Full" },
            { 0x06, "(0x06)No setting.ini"},
            { 0x07, "(0x07)Incorrect format"}
        };

        public override string AdditionalInfo()
        {
            return $"-> {GetMsg(ErrorData1Dic, Src.ErrorData1)} -> {GetMsg(ErrorData2Dic, Src.ErrorData2)}";
        }
    }

    public class ErrorDeviceLogStructure : BaseLogStructure
    {
        public ErrorDeviceLogStructure(LogSource src) : base(src) { }

        public static readonly Dictionary<int, string> ErrorData1Dic = new Dictionary<int, string>
        {
            {0x01, "(0x01)USB Dongle"},
            {0x02, "(0x02)SIM"}
        };

        public static readonly Dictionary<int, string> ErrorData2Dic = new Dictionary<int, string>
        {
            {0x01, "(0x01)Not Recognize-Insert"},
            {0x02, "(0x02)No Phone Number"},
            {0x03, "(0x03)Non-Activate"},
        };

        public override string AdditionalInfo()
        {
            return $"-> {GetMsg(ErrorData1Dic, Src.ErrorData1)} -> {GetMsg(ErrorData2Dic, Src.ErrorData2)}";
        }
    }

    public class ErrorNetworkLogStructure : BaseLogStructure
    {
        public ErrorNetworkLogStructure(LogSource src) : base(src) { }

        public static readonly Dictionary<int, string> ErrorData1Dic = new Dictionary<int, string>
        {
            {0x01, "(0x01)Network"},
        };

        public static readonly Dictionary<int, string> ErrorData2Dic = new Dictionary<int, string>
        {
            {0x01, "(0x01)Data Not Connect"},
            {0x02, "(0x02)Network Error"}
        };

        public override string AdditionalInfo()
        {
            return $"-> {GetMsg(ErrorData1Dic, Src.ErrorData1)} -> {GetMsg(ErrorData2Dic, Src.ErrorData2)}";
        }
    }

    public class ErrorSystemLogStructure : BaseLogStructure
    {
        public ErrorSystemLogStructure(LogSource src) : base(src) { }

        public static readonly Dictionary<int, string> ErrorData1Dic = new Dictionary<int, string>
        {
            {0x01, "(0x01)Device Error"}
        };

        public static readonly Dictionary<int, string> ErrorData2Dic = new Dictionary<int, string>
        {
            {0x02, "(0x02)GPS Error"}
        };

        public static readonly Dictionary<int, string> ErrorData3Dic = new Dictionary<int, string>
        {
            {0x01, "(0x01)GPS Open Fail RetryCount: "},
            {0x02, "(0x02)GPS Restart -> "}
        };

        public override string AdditionalInfo()
        {
            return
                $"-> {GetMsg(ErrorData1Dic, Src.ErrorData1)} -> {GetMsg(ErrorData2Dic, Src.ErrorData2)} -> {GetMsg(ErrorData3Dic, Src.ErrorData3)}{(Src.ErrorData4 == 0x01 ? "(0x01)MTK104 Cold Restart" : Src.ErrorData4.ToString())}";
        }
    }
}
