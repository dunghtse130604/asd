﻿using Newtonsoft.Json;
using System.Text;

namespace JB2.Core
{
    public static class JsonConvertExtensions
    {
        public static byte[] ToJsonBytes(this object obj)
        {
            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj));
        }

        public static byte[] ToJsonBytes(this object obj, JsonSerializerSettings settings)
        {
            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(obj, settings));
        }
    }
}