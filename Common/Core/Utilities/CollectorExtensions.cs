﻿using Microsoft.Azure.WebJobs;
using System.Threading.Tasks;

namespace JB2.Core
{
    public static class CollectorExtensions
    {
        public static async Task PushEventDataAsync<T>(this IAsyncCollector<T> collector, T data)
        {
            await collector.AddAsync(data);
            await collector.FlushAsync();
        }

        public static async Task PushQueueMessageAsync<T>(this IAsyncCollector<T> collector, T data)
        {
            await collector.AddAsync(data);
            await collector.FlushAsync();
        }
    }
}
