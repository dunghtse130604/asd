﻿using Microsoft.WindowsAzure.Storage.Table;

namespace JB2.Core
{
    public class LatestStartupInfo : TableEntity
    {
        public string sdId { get; set; }
        public string fw { get; set; }
        public string phone { get; set; }
        public int calibration { get; set; }
        public int angle { get; set; }

        public LatestStartupInfo(string deviceId)
        {
            PartitionKey = deviceId;
            RowKey = MessageType.Startup;
        }

        public LatestStartupInfo() { }
    }
}
