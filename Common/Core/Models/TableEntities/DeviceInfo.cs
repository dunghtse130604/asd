﻿using Microsoft.WindowsAzure.Storage.Table;

namespace JB2.Core
{
    public static class DeviceInfoSchema
    {
        public const string Valid = "valid";
        public const string NewFw = "newFw";
        public const string LastResetTime = "lastResetTime";
        public const string LastSentDriveTime = "lastSentDriveTime";
        public const string LastReceivedDriveTime = "lastReceivedDriveTime";
        public const string Setting = "setting";
        public const string VideoTime = "videoTime";
        public const string LastSystemLogIndex = "lastSystemLogIndex";
        public const string LastErrorLogIndex = "lastErrorLogIndex";
    }

    public class DeviceInfo<T> : TableEntity
    {
        public DeviceInfo() { }

        public T value { get; set; }

        public DeviceInfo(string deviceId, string param)
        {
            PartitionKey = deviceId;
            RowKey = param;
        }
        public DeviceInfo(string deviceId, string param, T value)
            : this(deviceId, param)
        {
            this.value = value;
        }
    }

    public class DeviceVideoTimeProperty : TableEntity
    {
        public DeviceVideoTimeProperty() { }
        public int? pre { get; set; }
        public int? post { get; set; }
        public DeviceVideoTimeProperty(string deviceId, string param) : base(deviceId, param) { }
    }
}
