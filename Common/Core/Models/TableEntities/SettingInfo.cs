﻿using Microsoft.WindowsAzure.Storage.Table;

namespace JB2.Core
{
    public static class SettingSchema
    {
        public const string ADAS = "ADAS";
        public const string VP_X = "VP_X";
        public const string VP_Y = "VP_Y";
        public const string BE_P = "BE_P";
        public const string HD_VP_X = "HD_VP_X";
        public const string HD_VP_Y = "HD_VP_Y";
        public const string HD_BE_P = "HD_BE_P";
        public const string CAR_HEIGHT = "CAR_HEIGHT";
        public const string FCW_ENABLE = "FCW_ENABLE";

        public static string[] VpcSettings = { VP_X, VP_Y, BE_P, HD_VP_X, HD_VP_Y, HD_BE_P };
    }

    public class SettingInfo : TableEntity
    {
        public string section { get; set; }
        public string key { get; set; }
        public string value { get; set; }

        public SettingInfo() { }

        public SettingInfo(string deviceId, string section, string key)
        {
            PartitionKey = deviceId;
            RowKey = section + "_" + key;
            this.section = section;
            this.key = key;
        }
        public SettingInfo(string deviceId, string section, string key, string value)
            : this(deviceId, section, key)
        {
            this.value = value;
        }
    }
}
