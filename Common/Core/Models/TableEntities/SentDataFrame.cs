﻿using Microsoft.WindowsAzure.Storage.Table;

namespace JB2.Core
{
    public class SentDataFrame : TableEntity
    {
        public string uuid { get; set; }

        public SentDataFrame(string deviceId, string time)
        {
            PartitionKey = deviceId;
            RowKey = time;
        }
        public SentDataFrame(string deviceId, string time, string uuid)
            : base(deviceId, time)
        {
            this.uuid = uuid;
        }

        public SentDataFrame() { }
    }
}
