﻿using Microsoft.WindowsAzure.Storage.Table;

namespace JB2.Core
{
    public class DataRequest : TableEntity
    {
        public long fromTime { get; set; }
        public long toTime { get; set; }
        public string requestSource { get; set; }

        public DataRequest() { }

        public DataRequest(string deviceId, long fromTime, long toTime, string requestSource)
        {
            PartitionKey = deviceId;
            RowKey = fromTime + "_" + toTime;
            this.fromTime = fromTime;
            this.toTime = toTime;
            this.requestSource = requestSource;
        }
    }
}
