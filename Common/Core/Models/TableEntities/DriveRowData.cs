﻿using Microsoft.WindowsAzure.Storage.Table;

namespace JB2.Core
{
    public class DriveRowData : TableEntity
    {
        public string content { get; set; }

        public DriveRowData() { }

        public DriveRowData(string deviceId, long time, string content = null)
        {
            PartitionKey = deviceId;
            RowKey = time.ToDatetimeString(TelemetryData.DateFormat);
            this.content = content;
        }
    }
}
