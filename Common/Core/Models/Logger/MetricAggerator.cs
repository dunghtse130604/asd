﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;

namespace JB2.Core
{
    public class MetricAggerator
    {
        private string procedureName;

        private List<double> metricValues = new List<double>();

        private bool Running { get; set; } = true;

        public MetricAggerator(string name, string instrusmentionKey, int intervalSecond = 30)
        {
            procedureName = name;
            var tc = new TelemetryClient
            {
                InstrumentationKey = instrusmentionKey
            };

            Task.Factory.StartNew(async () =>
            {
                while (Running)
                {
                    await Task.Delay(TimeSpan.FromSeconds(intervalSecond));
                    var metric = Aggregate();
                    if (metric != null)
                    {
                        tc.TrackMetric(metric);
                    }
                }
            }, TaskCreationOptions.LongRunning);
        }

        private MetricTelemetry Aggregate()
        {
            lock (this)
            {
                if (!metricValues.Any())
                {
                    return null;
                }

                var metric = new MetricTelemetry
                {
                    Timestamp = DateTime.UtcNow,
                    Name = procedureName,
                    Count = metricValues.Count,
                    Max = metricValues.Max(),
                    Min = metricValues.Min(),
                    Sum = metricValues.Sum(),
                };

                metricValues.Clear();
                return metric;
            }
        }

        public void Add(Stopwatch sw)
        {
            var value = sw.ElapsedMilliseconds;
            sw.Stop();
            lock (this)
            {
                metricValues.Add(value);
            }
        }

        public void Stop()
        {
            Running = false;
        }
    }

    public class GlobalMetricAggerator
    {
        private readonly Lazy<MetricAggerator> metricLazy;

        public GlobalMetricAggerator(string name)
        {
            metricLazy = new Lazy<MetricAggerator>(() => new MetricAggerator(name, AppEnvironment.AppInsightsKey));
        }

        public void Add(Stopwatch sw)
        {
            metricLazy.Value.Add(sw);
        }
    }
}
