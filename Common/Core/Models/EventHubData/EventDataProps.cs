﻿namespace JB2.Core
{
    public static class EventDataProps
    {
        public const string MessageType = "messageType";
        public const string SdId = "sdId";
        public const string SentTime = "sentTime";
        public const string Blob = "blob";
        public const string Content = "content";
        public const string DeviceId = "deviceId";
        public const string DataId = "dataId";
        public const string PoisonCount = "poisonCount";
        public const string ContentSize = "contentSize";
        public const string DataSize = "dataSize";
        public const string MediaSize = "mediaSize";
    }

    public static class MessageType
    {
        public const string Drive = "drive";
        public const string DriveData = "driveData";
        public const string Startup = "startup";
        public const string Event = "event";
        public const string EventData = "eventData";
        public const string Vpc = "vpc";
        public const string Log = "log";
        public const string Log2 = "log2";
        public const string Mdt = "mdt";
        public const string Reset = "reset";
        public const string Error = "error";
        public const string Setting = "setting";    //Abolished type
    }

    public static class DeviceLogType
    {
        public static int Error = 0x1000;
    }
}
