using Microsoft.Azure.Devices;
using Microsoft.Extensions.Logging;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace JB2.Core
{
    // EventData wrapper class with parsing some properties and body data commonly for IoTHub and EventHub data
    public class JB2Message
    {
        public EventData Data { get; protected set; }
        public string DeviceId { get; protected set; }
        public string SdId { get; protected set; }
        public string MessageType { get; protected set; }
        public long ReceivedTime => UnixTime.From(Data.EnqueuedTimeUtc);
        public IDictionary<string, object> Properties => Data.Properties;
        public bool IsOldVersion() => !Properties.ContainsKey(EventDataProps.ContentSize);
        public bool HasBlob() => Properties.ContainsKey(EventDataProps.Blob);

        // The following are avilable only IoTHub messages
        public long SentTime { get; protected set; }
        public string JsonContent { get; protected set; }
        public byte[] ExtraData { get; protected set; }
        public byte[] MediaData { get; protected set; }

        public int PoisonCount
        {
            get { return GetIntValue(EventDataProps.PoisonCount); }
            set { Properties[EventDataProps.PoisonCount] = value; }
        }

        private int GetIntValue(string key)
        {
            object value;
            return Properties.TryGetValue(key, out value) ? int.Parse(value.ToString()) : 0;
        }

        public JB2Message(EventData ev)
        {
            Data = ev;
        }

        public bool ParseData(ILogger logger)
        {
            try
            {
                DeviceId = Data.Properties[EventDataProps.DeviceId].ToString();
                SdId = Data.Properties[EventDataProps.SdId].ToString();
                MessageType = Data.Properties[EventDataProps.MessageType].ToString();
                return true;
            }
            catch (Exception ex)
            {
                logger = logger.AddDeviceIdProp(DeviceId);
                logger.Exception(ex);
                logger.Warning($"Unexpected EventHub message: {ex.Message}");
                return false;
            }
        }

        public bool ParseIoTData(ILogger logger, CloudBlobContainer iothubContainer = null)
        {
            try
            {
                object deviceId;
                Data.SystemProperties.TryGetValue(MessageSystemPropertyNames.ConnectionDeviceId, out deviceId);
                DeviceId = deviceId?.ToString() ?? Properties[EventDataProps.DeviceId].ToString();
                logger = logger.AddDeviceIdProp(DeviceId);

                // Temporal validation for the currupted sent data by TCU bug (Not output as exception)
                if (!Properties.ContainsKey(EventDataProps.SdId))
                {
                    logger.Warning("sdId is not contained");
                    return false;
                }
                SentTime = long.Parse(Properties[EventDataProps.SentTime].ToString());
                if (SentTime == -1)
                {
                    logger.Warning("sentTime is -1");
                    return false;
                }
                if (Math.Abs((DateTimeOffset.FromUnixTimeSeconds(SentTime).DateTime - Data.EnqueuedTimeUtc).TotalHours) > 1)
                {
                    logger.Warning($"Duration of (sentTime and receivedTime) > 1 hour. SentTime={SentTime}, ReceivedTime={Data.EnqueuedTimeUtc}");
                }

                MessageType = Properties[EventDataProps.MessageType].ToString();
                SdId = Properties[EventDataProps.SdId].ToString();

                if (IsOldVersion())
                {
                    // Compatible parsing for the old version
                    // If <contentSize> not exists, get json content data from <content> field.
                    JsonContent = Properties[EventDataProps.Content].ToString();

                    // In the old version, message body is wholly extra data
                    ExtraData = Data.GetBytes();
                }
                else
                {
                    using (var bodyStream = new MemoryStream(Data.GetBytes()))
                    using (var bodyReader = new BinaryReader(bodyStream))
                    {
                        bodyReader.BaseStream.Position = 0;
                        int contentSize = GetIntValue(EventDataProps.ContentSize);
                        int dataSize = GetIntValue(EventDataProps.DataSize);
                        int mediaSize = GetIntValue(EventDataProps.MediaSize);

                        //Abnormal case: Body length of message does not equal sum of properties size. Stop procedure
                        if (bodyStream.Length != contentSize + dataSize + (mediaSize == -1 ? 0 : mediaSize))
                        {
                            logger.Error("Body size of message does not equal sum of properties size: " +
                                                        $"(body={bodyStream.Length}, content={contentSize}, data={dataSize}, media={mediaSize})");
                            return false;
                        }

                        JsonContent = Encoding.UTF8.GetString(bodyReader.ReadBytes(contentSize));
                        if (dataSize != 0)
                        {
                            ExtraData = bodyReader.ReadBytes(dataSize);
                        }
                        if (mediaSize > 0)
                        {
                            MediaData = bodyReader.ReadBytes(mediaSize);
                        }
                    }
                }

                // If blob property exists, get the blob as media data. Check null, empty to prevent drop message,
                // when properties exist but do not have value
                object blobProp;
                if (Properties.TryGetValue(EventDataProps.Blob, out blobProp) 
                            && !string.IsNullOrEmpty(blobProp.ToString()))
                {
                    // download blob from iothub container
                    var blockBlob = iothubContainer.GetBlockBlobReference(DeviceId + "/" + blobProp.ToString());
                    if (!blockBlob.Exists())
                    {
                        // Drop the message if blob file not exist in storange
                        logger.Error($"{blockBlob.Name} not found in iothub container");
                        return false;
                    }
                    MediaData = blockBlob.DownloadByteArray();
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Exception(ex);
                logger.Error($"Unexpected IoTHub message: {ex.Message}");
                return false;
            }
        }
    }
}