﻿namespace JB2.Core
{
    public class EventProcessingMessage
    {
        public string deviceId { get; set; }
        public long fromTime { get; set; }
        public long toTime { get; set; }
        public string requestSource { get; set; }
    }
}
