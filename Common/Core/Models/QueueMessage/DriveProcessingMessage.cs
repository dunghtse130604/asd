﻿namespace JB2.Core
{
    public class DriveProcessingMessage
    {
        public string deviceId { get; set; }
        public string sdId { get; set; }
        public long time { get; set; }
        public string uuid { get; set; }
        public long engineOnTime { get; set; }

        public override string ToString()
        {
            return $"deviceId: {deviceId}, sdId: {sdId}, time: {time}, uuid: {uuid}, engineOnTime: {engineOnTime}";
        }
    }
}
