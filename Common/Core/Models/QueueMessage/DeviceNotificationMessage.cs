﻿namespace JB2.Core
{
    public class DeviceNotificationMessage
    {
        public static class ActionType
        {
            public const string OnInitialization = "OnInitialization";
            public const string OnSetting = "OnSetting";
            public const string OnMdtRequest = "OnMdtRequest";
            public const string OnFota = "OnFota";
        }

        public const string MessageType = "messageType";

        public string deviceId { get; set; }
        public string sdId { get; set; }
        public string actionType { get; set; }
        public string parameter { get; set; }

        public override string ToString()
        {
            return $"deviceId: {deviceId}, actionType: {actionType}, parameter: {parameter}";
        }
    }
}
