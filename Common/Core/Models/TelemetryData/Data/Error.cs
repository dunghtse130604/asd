﻿namespace JB2.Core
{
    public class Error : BaseData
    {
        public long time { get; set; }
        public int status { get; set; }
        public override string CreateId()
        {
            //Return as error_<time:YYYYMMDD_hhmmss>
            return "error_" + time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "error";
        }
    }
}
