﻿namespace JB2.Core
{
    public class EventData_ : Event
    {
        public Data data { get; set; }

        public override string CreateId()
        {
            //Return as eventData_<time:YYYYMMDD_hhmmss>
            return "eventData_" + time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "eventData";
        }
    }
}
