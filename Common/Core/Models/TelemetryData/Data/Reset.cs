﻿namespace JB2.Core
{
    public class Reset : BaseData
    {
        public long time { get; set; }
        public Gps gps { get; set; }
        public string photo { get; set; }

        public override string CreateId()
        {
            //Return as reset_<time:YYYYMMDD_hhmmss>
            return "reset_" + time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "reset";
        }
    }
}
