﻿namespace JB2.Core
{
    public class Mdt : BaseData
    {
        public long time { get; set; }
        public Video video { get; set; }

        public override string CreateId()
        {
            //Return as mdt_<time:YYYYMMDD_hhmmss>
            return "mdt_" + time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "mdt";
        }
    }
}
