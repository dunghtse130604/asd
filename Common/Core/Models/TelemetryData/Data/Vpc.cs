﻿namespace JB2.Core
{
    public class Vpc : BaseData
    {
        public long time { get; set; }
        public Gps gps { get; set; }
        public Result result { get; set; }
        public string photo { get; set; }

        public override string CreateId()
        {
            //Return as vpc_<time:yyyyMMdd_HHmmss>
            return "vpc_" + time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "vpc";
        }
    }
}
