﻿namespace JB2.Core
{
    public class Event : BaseData
    {
        public long time { get; set; }
        public Gps gps { get; set; }
        public Flags flags { get; set; }
        public int strength { get; set; }
        public string photo { get; set; }

        public override string CreateId()
        {
            //Return as event_<time:YYYYMMDD_hhmmss>
            return "event_" + time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "event";
        }
    }
}
