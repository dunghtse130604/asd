﻿namespace JB2.Core
{
    public class Startup : BaseData
    {
        public long time { get; set; }
        public Gps gps { get; set; }
        public string fw { get; set; }
        public string phone { get; set; }
        public Sensor sensor { get; set; }
        public int vpcReset { get; set; }
        public Fcw fcw { get; set; }

        public override string CreateId()
        {
            //Return as startup_<time:YYYYMMDD_hhmmss>
            return "startup_" + time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "startup";
        }
    }
}
