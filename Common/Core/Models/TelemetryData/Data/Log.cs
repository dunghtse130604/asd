﻿namespace JB2.Core
{
    public class Log : BaseData
    {
        public long time { get; set; }
        public int index { get; set; }
        public int type { get; set; }
        public string message { get; set; }
        public int error { get; set; }

        public override string CreateId()
        {
            //Return as log_<time:YYYYMMDD_hhmmss>
            return $"log_{time.ToDatetimeString(TelemetryData.DateFormat)}_{type}";
        }
        public override string MessageType()
        {
            return "log";
        }
    }
}
