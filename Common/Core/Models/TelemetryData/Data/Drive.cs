﻿namespace JB2.Core
{
    public class Drive : BaseData
    {
        public long engineOnTime { get; set; }
        public long time { get; set; }
        public int duration { get; set; }
        public LastGps lastGps { get; set; }

        public override string CreateId()
        {
            //Return as drive_<time:YYYYMMDD_hhmmss>
            return "drive_" + time.ToDatetimeString(TelemetryData.DateFormat);
        }
        public override string MessageType()
        {
            return "drive";
        }
    }
}
