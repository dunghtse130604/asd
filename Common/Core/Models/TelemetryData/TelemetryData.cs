﻿using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace JB2.Core
{
    public class TelemetryData
    {
        public const string DatabaseName = "jb2-db";
        public const string CollectionName = "TelemetryData";
        public const string DateFormat = "yyyyMMdd_HHmmss";
    }

    public abstract class BaseData
    {
        public abstract string CreateId();
        public abstract string MessageType();
    }

    public class TelemetryData<T> where T : BaseData
    {
        public TelemetryData(string deviceId, string sdId, long sentTime, long receivedTime, T data, int? timeToLive)
        {
            this.deviceId = deviceId;
            this.sdId = sdId;
            this.sentTime = sentTime;
            this.receivedTime = receivedTime;
            this.messageType = data.MessageType();
            this.data = data;
            this.id = data.CreateId();
            this.timeToLive = timeToLive;
        }

        public string deviceId { get; set; }
        public string sdId { get; set; }
        public long sentTime { get; set; }
        public long receivedTime { get; set; }
        public string messageType { get; set; }
        public string id { get; set; }
        public T data { get; set; }
        [JsonProperty(PropertyName = "ttl", NullValueHandling = NullValueHandling.Ignore)]
        public int? timeToLive { get; set; }
    }

    public static class TelemetryExtension
    {
        public static IQueryable<TelemetryData<T>> TelemetryQuery<T>(this DocumentClient dbClient, string deviceId) where T : BaseData
        {
            var queryOptions = new FeedOptions
            {
                PartitionKey = new PartitionKey(deviceId),
                EnableScanInQuery = false,
                EnableCrossPartitionQuery = false,
                MaxItemCount = -1
            };
            return dbClient.CreateDocumentQuery<TelemetryData<T>>(
                UriFactory.CreateDocumentCollectionUri(TelemetryData.DatabaseName, TelemetryData.CollectionName),
                queryOptions);
        }

        public static async Task UpsertTelemetryAsync<T>(this DocumentClient dbClient,
            string deviceId, string sdId, long sentTime, long receivedTime, T data, int? timeToLive = null) where T : BaseData

        {
            var telemetryData = new TelemetryData<T>(deviceId, sdId, sentTime, receivedTime, data, timeToLive);
            await dbClient.UpsertDocumentAsync(
                UriFactory.CreateDocumentCollectionUri(TelemetryData.DatabaseName, TelemetryData.CollectionName),
                telemetryData, null, true);
        }

        public static async Task CreateTelemetryAsync<T>(this DocumentClient dbClient,
            string deviceId, string sdId, long sentTime, long receivedTime, T data, int? timeToLive=null) where T : BaseData
        {
            var telemetryData = new TelemetryData<T>(deviceId, sdId, sentTime, receivedTime, data, timeToLive);
            await dbClient.CreateDocumentAsync(
                UriFactory.CreateDocumentCollectionUri(TelemetryData.DatabaseName, TelemetryData.CollectionName),
                telemetryData, null, true);
        }
    }

}
