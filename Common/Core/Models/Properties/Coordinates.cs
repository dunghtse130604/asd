﻿namespace JB2.Core
{
    public class Coordinates
    {
        public int x { get; set; }
        public int y { get; set; }
        public int z { get; set; }
    }
}
