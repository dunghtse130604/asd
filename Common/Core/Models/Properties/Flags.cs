﻿namespace JB2.Core
{
    public class Flags
    {
        public int button { get; set; }
        public int shock { get; set; }
        public int accel { get; set; }
        public int brake { get; set; }
        public int turn { get; set; }
        public int fcw { get; set; }
    }
}
