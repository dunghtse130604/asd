﻿namespace JB2.Core
{
    public class Video
    {
        public int width { get; set; }
        public int height { get; set; }
        public int fps { get; set; }
        public int bitrate { get; set; }
    }
}
