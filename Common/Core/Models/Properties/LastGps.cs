﻿using Newtonsoft.Json;

namespace JB2.Core
{
    public class LastGps
    {
        public long time { get; set; }

        [JsonConverter(typeof(DecimalJsonConverter))]
        public double latitude { get; set; }

        [JsonConverter(typeof(DecimalJsonConverter))]
        public double longitude { get; set; }
        public int speed { get; set; }
        public int heading { get; set; }
    }
}
