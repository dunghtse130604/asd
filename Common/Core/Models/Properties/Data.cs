﻿namespace JB2.Core
{
    public class Data
    {
        public long time { get; set; }
        public int duration { get; set; }
        public string video { get; set; }
        public string sensor { get; set; }
    }
}
