﻿namespace JB2.Core
{
    public class Sensor
    {
        public int calibration { get; set; }
        public int angle { get; set; }
    }
}
