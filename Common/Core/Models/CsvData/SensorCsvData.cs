﻿namespace JB2.Core
{
    public class SensorCsvData : BaseCsv
    {
        public override string Header
        {
            get { return "deviceId\tsdId\ttime\tg.x\tg.y\tg.z\tgyro.x\tgyro.y\tgyro.z\n"; }
        }

        public static class Index
        {
            public const int DeviceId = 0;
            public const int SdId = 1;
            public const int Time = 2;
            public const int GX = 3;
            public const int GY = 4;
            public const int GZ = 5;
            public const int GyroX = 6;
            public const int GyroY = 7;
            public const int GyroZ = 8;
        }
        public const int MaxNum = 9;
    }
}
