﻿using System;
using System.Collections.Generic;

namespace JB2.Core
{
    public class DriveCsvData : BaseCsv
    {
        public static class Index
        {
            public const int DeviceId = 0;
            public const int SdId = 1;
            public const int EngineOnTime = 2;
            public const int Time = 3;
            public const int GpsValid = 4;
            public const int GpsLatitude = 5;
            public const int GpsLongitude = 6;
            public const int GpsSpeed = 7;
            public const int GpsHeading = 8;
            public const int GAveX = 9;
            public const int GAveY = 10;
            public const int GAveZ = 11;
            public const int GMaxX = 12;
            public const int GMaxY = 13;
            public const int GMaxZ = 14;
            public const int GMinX = 15;
            public const int GMinY = 16;
            public const int GMinZ = 17;
            public const int GyroAveX = 18;
            public const int GyroAveY = 19;
            public const int GyroAveZ = 20;
        }
        public const int MaxNum = 21;

        public long Time => long.Parse(Cells[Index.Time]);
        public int GpsValid => int.Parse(Cells[Index.GpsValid]);
        public double GpsLatitude => double.Parse(Cells[Index.GpsLatitude]);
        public double GpsLongitude => double.Parse(Cells[Index.GpsLongitude]);
        public int GpsSpeed => int.Parse(Cells[Index.GpsSpeed]);
        public int GpsHeading => int.Parse(Cells[Index.GpsHeading]);
    }

    public class OutputDriveCsvData : BaseCsv
    {
        public override string Header {
            get { return "deviceId\tsdId\tengineOnTime\ttime\tgps.valid\tgps.latitude\tgps.longitude\tgps.speed\tgps.heading\test.speed\test.fbAccel\test.lrAccel\tg.ave.x\tg.ave.y\tg.ave.z\tg.max.x\tg.max.y\tg.max.z\tg.min.x\tg.min.y\tg.min.z\n"; }
        }

        public static class Index
        {
            public const int DeviceId = 0;
            public const int SdId = 1;
            public const int EngineOnTime = 2;
            public const int Time = 3;
            public const int GpsValid = 4;
            public const int GpsLatitude = 5;
            public const int GpsLongitude = 6;
            public const int GpsSpeed = 7;
            public const int GpsHeading = 8;
            public const int EstSpeed = 9;
            public const int EstFBAccel = 10;
            public const int EstLRAccel = 11;
            public const int GAveX = 12;
            public const int GAveY = 13;
            public const int GAveZ = 14;
            public const int GMinX = 15;
            public const int GMinY = 16;
            public const int GMinZ = 17;
            public const int GMaxX = 18;
            public const int GMaxY = 19;
            public const int GMaxZ = 20;
        }
        public const int MaxNum = 21;

        public OutputDriveCsvData(DriveCsvData srcData, IReadOnlyList<double?> estData)
        {
            ReserveCells(MaxNum);
            Array.Copy(srcData.Cells, Cells, DriveCsvData.Index.GAveX);
            Cells[Index.EstSpeed] = FloatString(estData[0]);
            Cells[Index.EstFBAccel] = FloatString(estData[1]);
            Cells[Index.EstLRAccel] = FloatString(estData[2]);
            Array.Copy(srcData.Cells, DriveCsvData.Index.GAveX, Cells, Index.GAveX, MaxNum - Index.GAveX);  // TODO: With a bug of wrong order of Min and Max values
        }

        private static string FloatString(double? d)
        {
            return d != null ? d.Value.ToString("0.########") : string.Empty;
        }

    }
}