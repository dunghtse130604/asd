﻿using System;

namespace JB2.Core
{
    public static class AppEnvironment
    {
        public static string IoTHubConnectionString => Environment.GetEnvironmentVariable("IoTHubConnectionString");
        public static string RestApiAccessKey1 => Environment.GetEnvironmentVariable("AccessKey1");
        public static string RestApiAccessKey2 => Environment.GetEnvironmentVariable("AccessKey2");
        public static string RestApiURL => Environment.GetEnvironmentVariable("ApiUrl");
        public static string RedisDbConnectionString => Environment.GetEnvironmentVariable("RedisDbConnectionString");
        public static string DriveCalculatorUrl => Environment.GetEnvironmentVariable("DriveCaculatorUrl");
        public static string DeviceConnectionString => Environment.GetEnvironmentVariable("DeviceConnStr");
        public static string ConfigurationApiUrl => Environment.GetEnvironmentVariable("ConfigurationApiUrl");
        public static string AppInsightsKey => Environment.GetEnvironmentVariable("APPINSIGHTS_INSTRUMENTATIONKEY");
    }
}