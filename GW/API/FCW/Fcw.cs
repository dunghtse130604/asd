using JB2.Core;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JB2.API
{
    public class Fcw
    {
        protected Fcw() { }
        private const string JsonMediaType = "application/json";

        public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, string deviceId, ILogger logger,
            CloudTable outDeviceInfo, CloudTable outSettingInfo, CloudTable inLatestInfo, IAsyncCollector<DeviceNotificationMessage> deviceNotificationQueueItems)
        {
            logger = logger.AddDeviceIdProp(deviceId);
            // 2.Authentication validate
            // Check Security(Date,Content-MD5, X-Signature)
            if (!await SecurityUtils.IsAuthenticatedAsync(req, logger))
            {
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }

            // Check deviceId exist in LatestInfo table - this table contains all devices from kitting.
            var latestInfo = await inLatestInfo.RetrieveEntityAsync(new LatestStartupInfo(deviceId));
            if (latestInfo == null)
            {
                logger.Warning("DeviceId is not registered");
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            // Get heightValue
            var heightValue = (await outSettingInfo.RetrieveEntityAsync(
                new SettingInfo(deviceId, SettingSchema.ADAS, SettingSchema.CAR_HEIGHT)))?.value;
            // Get alertValue
            var alertValue = (await outSettingInfo.RetrieveEntityAsync(
                new SettingInfo(deviceId, SettingSchema.ADAS, SettingSchema.FCW_ENABLE)))?.value;

            if (req.Method == HttpMethod.Put)
            {
                string newAlert = null;
                string newHeight = null;
                try
                {
                    var requestBody = await req.Content.ReadAsStringAsync();
                    logger.Info($"Fcw request body: {requestBody}");
                    // Parse request body
                    ParseRequestBody(requestBody, ref newAlert, ref newHeight);
                }
                catch (Exception e)
                {
                    //Catch request body format invalid
                    logger.Warning("Invalid body request: " + e.Message);
                    return new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent(e.Message)
                    };
                }

                // If<height> exists in the request data
                if (newHeight != null)
                {
                    // Get all entry for this deviceId from DeviceInfo table storage (filter data purpose)
                    var settingInfoItems = outSettingInfo.QueryDeviceEntities<SettingInfo>(deviceId);

                    // 3.If <height> exists in the request data, Execute [VPC reset]
                    try
                    {
                        await HandleResetVpc(outDeviceInfo, outSettingInfo, deviceId, settingInfoItems);
                    }
                    catch (StorageException e)
                    {
                        //Do not throw exception in case not found 
                        if (e.RequestInformation.HttpStatusCode != 404)
                        {
                            throw;
                        }
                    }

                    // 4.Settingnfo table:  setting_ADAS_CAR_HEIGHT record with the new parameter
                    heightValue = newHeight;
                    await outSettingInfo.UpsertEntityAsync(
                        new SettingInfo(deviceId, SettingSchema.ADAS, SettingSchema.CAR_HEIGHT, newHeight));

                    // 4.DeviceNotification queue:  OnSetting message with null
                    var notificationMsg = new DeviceNotificationMessage
                    {
                        deviceId = deviceId,
                        actionType = DeviceNotificationMessage.ActionType.OnSetting,
                        parameter = null
                    };
                    await deviceNotificationQueueItems.PushQueueMessageAsync(notificationMsg);
                }

                //If<alert> exists in the request data
                if (newAlert != null)
                {
                    alertValue = newAlert;
                    // 4.SettingInfo table :  setting_ADAS_FCW_ENABLE record with the new parameter
                    await outSettingInfo.UpsertEntityAsync(
                        new SettingInfo(deviceId, SettingSchema.ADAS, SettingSchema.FCW_ENABLE, newAlert));
                }
            }

            // Return <height> and <alert> data 
            var responseBody = JsonConvert.SerializeObject(new
            {
                height = heightValue != null ? int.Parse(heightValue) : 0,
                alert = alertValue != null ? int.Parse(alertValue) : 0
            });
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(responseBody, Encoding.UTF8, JsonMediaType)
            };
        }

        private static void ParseRequestBody(string reqContent, ref string fcwAlert, ref string fcwHeight)
        {
            // Get request body
            var requestObj = JsonConvert.DeserializeObject<dynamic>(reqContent);

            // Check request body format invalid
            if (requestObj == null)
            {
                throw new JsonException("Invalid json format");
            }

            if (requestObj.height != null)
            {
                int outHeight;
                if (!int.TryParse(requestObj.height.Value.ToString(), out outHeight))
                {
                    throw new JsonException("Invalid value 'height'");
                }
                fcwHeight = Convert.ToString(outHeight);
            }
            if (requestObj.alert != null)
            {
                var reqAlert = requestObj.alert.Value.ToString();
                if (!"0".Equals(reqAlert) && !"1".Equals(reqAlert))
                {
                    throw new JsonException("Invalid value 'alert'");
                }
                fcwAlert = reqAlert;
            }
        }

        private static async Task HandleResetVpc(CloudTable outDeviceInfo, CloudTable outSettingInfoTable,
            string deviceId, IList<SettingInfo> settingInfoItems)
        {
            // Update lastResetTime Item
            await outDeviceInfo.UpsertEntityAsync(
                new DeviceInfo<long>(deviceId, DeviceInfoSchema.LastResetTime, UnixTime.Now));

            // Remove ADAS setting from SettingInfo table.
            var operations = new TableBatchOperation();
            settingInfoItems
                .Where(d => d.section == SettingSchema.ADAS && SettingSchema.VpcSettings.Contains(d.key))
                .ToList()
                .ForEach(item => { operations.Delete(item); });
            if (operations.Any())
            {
                await outSettingInfoTable.ExecuteBatchAsync(operations);
            }
        }
    }
}