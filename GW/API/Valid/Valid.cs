using JB2.Core;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JB2.API
{
    public class Valid
    {
        static Valid() { new VersionLogger(); }

        private const string JsonMediaType = "application/json";

        public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, string deviceId,
            DeviceInfo<int> validEntity, CloudTable outDeviceInfo, CloudTable outSettingInfo, CloudTable inLatestInfo,
            IAsyncCollector<DeviceNotificationMessage> deviceNotificationQueueItems, ILogger logger)
        {
            logger = logger.AddDeviceIdProp(deviceId);
            // 1. If authentication is invalid, return with 401
            if (!await SecurityUtils.IsAuthenticatedAsync(req, logger))
            {
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }

            // Check deviceId exist in LatestInfo table - this table contains all devices from kitting.
            var latestInfo = await inLatestInfo.RetrieveEntityAsync(new LatestStartupInfo(deviceId));
            if (latestInfo == null)
            {
                logger.Warning("DeviceId is not registered");
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            // 2.Get valid value
            int valid = 0; //Default value
            if (validEntity != null)
            {
                valid = validEntity.value;
            }

            if (req.Method == HttpMethod.Put)
            {
                try
                {
                    // Parse request body
                    var requestBody = await req.Content.ReadAsStringAsync();
                    logger.Info($"Valid request body: {requestBody}");
                    valid = ParseRequestBody(requestBody);
                }
                catch (Exception e)
                {
                    //Catch request body format invalid
                    logger.Warning("Invalid body request: " + e.Message);
                    return new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent(e.Message, Encoding.UTF8)
                    };
                }

                // 3.If new valid is 0 
                // -> Remove setting.ADAS_FCW_ENABLE record from SettingInfo table
                if (valid == 0)
                {
                    try
                    {
                        await outSettingInfo.DeleteEntityAsync(
                            new SettingInfo(deviceId, SettingSchema.ADAS, SettingSchema.FCW_ENABLE));

                        var notificationMsg = new DeviceNotificationMessage
                        {
                            deviceId = deviceId,
                            actionType = DeviceNotificationMessage.ActionType.OnSetting,
                            parameter = null
                        };
                        await deviceNotificationQueueItems.PushQueueMessageAsync(notificationMsg);
                    }
                    catch (StorageException ex)
                    {
                        if (ex.RequestInformation.HttpStatusCode != (int)HttpStatusCode.NotFound)
                        {
                            throw;
                        }
                    }
                }

                // 4.Update valid record with the new parameter
                if (validEntity == null)
                {
                    // If existing entity not exists, create new entity
                    await outDeviceInfo.UpsertEntityAsync(
                        new DeviceInfo<int>(deviceId, DeviceInfoSchema.Valid, valid));
                }
                else
                {
                    // Update value of existing entity 
                    validEntity.value = valid;
                }
            }

            // 5.Return valid value
            var responseBody = JsonConvert.SerializeObject(new { valid });
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(responseBody, Encoding.UTF8, JsonMediaType)
            };
        }

        private static int ParseRequestBody(string reqContent)
        {
            // Get request body
            var requestObj = JsonConvert.DeserializeObject<dynamic>(reqContent);
            if (requestObj == null)
            {
                throw new JsonException("Invalid json format");
            }

            // Check request body format invalid
            if (requestObj.valid == null)
            {
                throw new JsonException("'valid' field is not found");
            }

            int validValue = int.Parse(Convert.ToString(requestObj.valid.Value));
            if (!(validValue == 0 || validValue == 1))
            {
                throw new JsonException("Invalid value 'valid'");
            }
            return validValue;
        }
    }
}