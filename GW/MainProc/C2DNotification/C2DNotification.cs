﻿using JB2.Core;
using Microsoft.Azure.Devices;
using Microsoft.Azure.Devices.Common.Exceptions;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace JB2.MainProc
{
    public class C2DNotification
    {
        protected C2DNotification()
        {
        }

        private static readonly Lazy<ServiceClient> lazyServiceClient = new Lazy<ServiceClient>(() =>
            ServiceClient.CreateFromConnectionString(AppEnvironment.IoTHubConnectionString));

        private static ServiceClient serviceClient => lazyServiceClient.Value;


        public static async Task Run(DeviceNotificationMessage queueItem, CloudTable deviceInfo,
            DocumentClient dbClient,
            CloudTable settingInfo, CloudBlobDirectory blobDirectory, ILogger logger, ExecutionContext context, DateTimeOffset insertionTime)
        {
            logger = logger.AddDeviceIdProp(queueItem.deviceId);
            logger.Info($"Notify device: Input={queueItem}, InsertionTime={insertionTime}");

            object data;
            switch (queueItem.actionType)
            {
                case DeviceNotificationMessage.ActionType.OnInitialization:
                    data = await HandleInitialization(queueItem.deviceId, queueItem.sdId, deviceInfo, settingInfo, logger,
                        dbClient);
                    break;
                case DeviceNotificationMessage.ActionType.OnSetting:
                    data = new { };
                    break;
                case DeviceNotificationMessage.ActionType.OnMdtRequest:
                    data = HandleMdtRequest(queueItem.parameter, logger);
                    break;
                case DeviceNotificationMessage.ActionType.OnFota:
                    data = await HandleOnFota(queueItem.parameter, blobDirectory, logger);
                    break;
                default:
                    logger.Warning($"Unhandle action: {queueItem.actionType}");
                    return;
            }
            if (data == null)
            {
                logger.Warning($"Data return null with: {queueItem}. Do not request device!");
                return;
            }

            var methodInvocation = new CloudToDeviceMethod(queueItem.actionType, TimeSpan.FromSeconds(15), TimeSpan.FromSeconds(15));
            methodInvocation.SetPayloadJson(JsonConvert.SerializeObject(data));

            // Direct method
            try
            {
                await serviceClient.InvokeDeviceMethodAsync(queueItem.deviceId, methodInvocation);
            }
            catch (Exception e)
            {
                if (IsTimeoutException(e))
                {
                    logger.Info($"DirectMethod timeout: {e.Message}");
                }
                else if (IsDeviceNotResponse(e))
                {
                    logger.Info($"DirectMethod not response: {e.Message}");
                }
                else if (IsAlreadyActiveConnection(e))
                {
                    logger.Info($"DirectMethod already active connection: {e.Message}");
                }
                else
                {
                    logger.Exception(e);
                }
            }
        }

        private static async Task<object> HandleInitialization(string deviceId, string sdId, CloudTable deviceInfo,
            CloudTable settingInfo, ILogger logger, DocumentClient inDbClient)
        {
            // Get time of the latest document from DriveData collection. If not exist, set 0.
            var lastDriveTime = (await deviceInfo.RetrieveEntityAsync(
                new DeviceInfo<long>(deviceId, DeviceInfoSchema.LastReceivedDriveTime)))?.value ?? 0;
            var lastSysLog = await ConfigurationApi.GetLogIndex(deviceInfo, inDbClient, deviceId, DeviceInfoSchema.LastSystemLogIndex);
            var lastErrLog = await ConfigurationApi.GetLogIndex(deviceInfo, inDbClient, deviceId, DeviceInfoSchema.LastErrorLogIndex);

            var fcwEnable = (await settingInfo.RetrieveEntityAsync(
                new SettingInfo(deviceId, SettingSchema.ADAS, SettingSchema.FCW_ENABLE)))?.value;

            var vpXEnt = await settingInfo.RetrieveEntityAsync(new SettingInfo(deviceId, SettingSchema.ADAS, SettingSchema.VP_X));

            // valid = 1 and setting.ADAS.VP_X not exists
            var vpcEnabled = fcwEnable == "1" && vpXEnt == null ? 1 : 0;

            // Get time of mdt documents where [recievedTime = 0] from TelemetryData.
            var mdtRequestList = inDbClient.TelemetryQuery<Mdt>(deviceId)
                .Where(s => s.receivedTime == 0 && s.messageType == MessageType.Mdt).ToList();
            var mdtRequest = string.Join(",", mdtRequestList.Select(x => x.data.time));

            var eventPhoto = sdId.StartsWith(ExternalConfig.KittingSdId) ? 1 : 0;
            var initData = new
            {
                lastDriveTime,
                lastSysLog,
                lastErrLog,
                vpcEnabled,
                vpcParam = GetVpcParam(),
                eventPhoto,
                mdtRequest
            };

            return initData;
        }

        private static object HandleMdtRequest(string parameter, ILogger logger)
        {
            if (string.IsNullOrEmpty(parameter) || parameter == ",")
            {
                logger.Warning("Param is invalid");
                return null;
            }

            var mdtObject = new
            {
                mdtRequest = parameter
            };
            return mdtObject;
        }

        private static async Task<object> HandleOnFota(string parameter, CloudBlobDirectory blobDirectory, ILogger logger)
        {
            //get Name file
            var fwName = parameter + ".img";

            // get url
            var fwUrl = new Uri($"{blobDirectory.Uri}/{fwName}").AbsoluteUri;

            var blob = blobDirectory.GetBlobReference(fwName);
            if (!await blob.ExistsAsync())
            {
                logger.Warning($"Firmware URL: {fwUrl} does not exist. Stop OnFota!");
                return null;
            }
            await blob.FetchAttributesAsync();
            // get fwSize
            var fwSize = blob.Properties.Length;

            // get ContentMD5 and covert to hex code(32 characters)
            var md5Bytes = Convert.FromBase64String(blob.Properties.ContentMD5);
            var fwMd5Sum = BitConverter.ToString(md5Bytes).Replace("-", "").ToLower();

            var onFotaObject = new
            {
                fwUrl,
                fwName,
                fwSize,
                fwMd5Sum
            };

            return onFotaObject;
        }

        public static object GetVpcParam()
        {
            //Function must make sure environment contains these environment variable
            return new
            {
                interval = ExternalConfig.VpcInterval,
                speed = ExternalConfig.VpcSpeed,
                maxNum = ExternalConfig.VpcMaxNum
            };
        }

        public static bool IsTimeoutException(Exception e)
        {
            return e is DeviceNotFoundException;
        }

        public static bool IsDeviceNotResponse(Exception e)
        {
            //IoTbhub exception error code: 504101 happen when device can connect to IoThub, but DO NOT response when recevei message from IoT
            //Use method string.Contains(): \"errorCode\":504101 to detect, because IotHubException instance does not have properties to store 504101 code
            var iotHubException = e as IotHubException;
            return iotHubException != null && iotHubException.Message.Contains(@"\""errorCode\"":504101");
        }

        public static bool IsAlreadyActiveConnection(Exception e)
        {
            //Argument exception error code: 400027 happen when the application activate DeviceTwin and DirectMethod concurrently using same as device ID
            //IoT hub does not accept more than 1 active connection at a time, so the later connection will throw 400027 error status code.
            var argumentException = e as ArgumentException;
            return argumentException != null && argumentException.Message.Contains(@"\""errorCode\"":400027");
        }
    }
}