using FFmpeg.AutoGen;
using System;
using System.CodeDom.Compiler;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

namespace JB2.MainProc
{
    public enum ImageType
    {
        YUV,
        JPEG
    }

    public class H264Converter
    {
        public class ConvertMediaException : ArgumentException
        {
            public ConvertMediaException(string msg) : base(msg) { }
        }

        private static H264Converter instance;
        private static readonly object locker = new object();

        public static H264Converter GetInstance(string path)
        {
            lock (locker)
            {
                if (instance == null)
                {
                    instance = new H264Converter(path);
                }
            }
            return instance;
        }

        protected H264Converter(string path)
        {
            InteropHelper.RegisterLibrariesSearchPath(
                $@"{path}/../bin/{(Environment.Is64BitProcess ? "x64" : "x86")}");
                ffmpeg.av_register_all();
                ffmpeg.avcodec_register_all();
                ffmpeg.avformat_network_init();
        }

        public unsafe byte[] ConvertToImage(byte[] inputBytes, ImageType imageType, int maxFrames = 1)
        {
            using (var tempCollection = new TempFileCollection())
            {
                var pFormatContext = ffmpeg.avformat_alloc_context();
                var inFile = tempCollection.AddExtension("dat");
                File.WriteAllBytes(inFile, inputBytes);
                // Open input
                if (ffmpeg.avformat_open_input(&pFormatContext, inFile, null, null) != 0)
                    throw new ConvertMediaException("Could not open file.");
                // Find stream info
                if (ffmpeg.avformat_find_stream_info(pFormatContext, null) != 0)
                    throw new ConvertMediaException("Could not find stream info.");
                AVStream* pStream = null;
                for (var i = 0; i < pFormatContext->nb_streams; i++)
                {
                    if (pFormatContext->streams[i]->codec->codec_type == AVMediaType.AVMEDIA_TYPE_VIDEO)
                    {
                        pStream = pFormatContext->streams[i];
                        break;
                    }
                }
                // Get stream
                if (pStream == null)
                    throw new ConvertMediaException("Could not found video stream.");
                var codecContext = *pStream->codec;
                var width = codecContext.width;
                var height = codecContext.height;
                var sourcePixFmt = codecContext.pix_fmt;
                // Check source format to prevent app crashing
                if (sourcePixFmt == AVPixelFormat.AV_PIX_FMT_NONE)
                    throw new ConvertMediaException("Could not detect source format.");
                var codecId = codecContext.codec_id;
                // Set output format
                var destinationPixFmt = imageType == ImageType.JPEG
                    ? AVPixelFormat.AV_PIX_FMT_BGR24
                    : AVPixelFormat.AV_PIX_FMT_NV12;
                var pConvertContext = ffmpeg.sws_getContext(width, height, sourcePixFmt,
                    width, height, destinationPixFmt,
                    ffmpeg.SWS_FAST_BILINEAR, null, null, null);
                if (pConvertContext == null)
                    throw new ConvertMediaException("Could not initialize the conversion context.");

                var pConvertedFrame = ffmpeg.av_frame_alloc();
                var convertedFrameBufferSize = ffmpeg.av_image_get_buffer_size(destinationPixFmt, width, height, 1);
                var convertedFrameBufferPtr = Marshal.AllocHGlobal(convertedFrameBufferSize);
                var dstData = new byte_ptrArray4();
                var dstLinesize = new int_array4();
                ffmpeg.av_image_fill_arrays(ref dstData, ref dstLinesize, (byte*) convertedFrameBufferPtr,
                    destinationPixFmt, width, height, 1);

                var pCodec = ffmpeg.avcodec_find_decoder(codecId);
                if (pCodec == null)
                    throw new ConvertMediaException("Unsupported codec.");

                var pCodecContext = &codecContext;

                if ((pCodec->capabilities & ffmpeg.AV_CODEC_CAP_TRUNCATED) == ffmpeg.AV_CODEC_CAP_TRUNCATED)
                    pCodecContext->flags |= ffmpeg.AV_CODEC_FLAG_TRUNCATED;

                if (ffmpeg.avcodec_open2(pCodecContext, pCodec, null) < 0)
                    throw new ConvertMediaException("Could not open codec.");

                var pDecodedFrame = ffmpeg.av_frame_alloc();

                var packet = new AVPacket();
                var pPacket = &packet;
                ffmpeg.av_init_packet(pPacket);
                var frameNumber = 0;
                while (frameNumber < maxFrames)
                {
                    try
                    {
                        if (ffmpeg.av_read_frame(pFormatContext, pPacket) < 0)
                            throw new ConvertMediaException("Could not read frame.");

                        if (pPacket->stream_index != pStream->index)
                            continue;

                        if (ffmpeg.avcodec_send_packet(pCodecContext, pPacket) < 0)
                            throw new ConvertMediaException($"Error while sending packet {frameNumber}.");

                        if (ffmpeg.avcodec_receive_frame(pCodecContext, pDecodedFrame) < 0)
                            throw new ConvertMediaException($"Error while receiving frame {frameNumber}.");

                        ffmpeg.sws_scale(pConvertContext, pDecodedFrame->data, pDecodedFrame->linesize, 0, height,
                            dstData, dstLinesize);
                    }
                    finally
                    {
                        ffmpeg.av_packet_unref(pPacket);
                        ffmpeg.av_frame_unref(pDecodedFrame);
                    }
                    frameNumber++;
                }

                byte[] managedArray;
                switch (imageType)
                {
                    case ImageType.YUV:
                        managedArray = new byte[width * height * 3 / 2];
                        Marshal.Copy(convertedFrameBufferPtr, managedArray, 0, managedArray.Length);
                        break;
                    case ImageType.JPEG:
                        using (var bitmap = new Bitmap(width, height, dstLinesize[0],
                            System.Drawing.Imaging.PixelFormat.Format24bppRgb, convertedFrameBufferPtr))
                        using (var ms = new MemoryStream())
                        {
                            bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                            managedArray = ms.ToArray();
                        }
                        break;
                    default:
                        throw new ArgumentException($"Not supported convert to {imageType.ToString()}");
                }

                Marshal.FreeHGlobal(convertedFrameBufferPtr);
                ffmpeg.av_free(pConvertedFrame);
                ffmpeg.sws_freeContext(pConvertContext);

                ffmpeg.av_free(pDecodedFrame);
                ffmpeg.avcodec_close(pCodecContext);
                ffmpeg.avformat_close_input(&pFormatContext);
                return managedArray;
            }
        }

        /// <summary>
        /// Convert H264 to Mp4
        /// </summary>
        /// <param name="inputBytes">H264 data</param>
        /// <param name="fps">fps value</param>
        /// <returns>Mp4 data</returns>
        public unsafe byte[] ConvertToVideo(byte[] inputBytes, int fps = 10)
        {
            using (var tempCollection = new TempFileCollection())
            {
                AVFormatContext* inFormatContext = null;
                AVFormatContext* outFormatContext = null;
                AVDictionary* options = null;
                ffmpeg.av_dict_set(&options, "framerate", fps.ToString(), 0);

                var inFile = tempCollection.AddExtension("dat");
                var outFile = tempCollection.AddExtension("mp4");
                File.WriteAllBytes(inFile, inputBytes);

                // Open input
                if (ffmpeg.avformat_open_input(&inFormatContext, inFile, null, &options) < 0)
                    throw new ConvertMediaException("Could not open file.");
                // Find stream info
                if (ffmpeg.avformat_find_stream_info(inFormatContext, null) < 0)
                    throw new ConvertMediaException("Could not find stream info.");

                AVStream* inStream = null;
                for (var i = 0; i < inFormatContext->nb_streams; i++)
                {
                    if (inFormatContext->streams[i]->codec->codec_type == AVMediaType.AVMEDIA_TYPE_VIDEO)
                    {
                        inStream = inFormatContext->streams[i];
                        break;
                    }
                }
                // Get stream
                if (inStream == null)
                    throw new ConvertMediaException("Could not found input stream.");

                if (ffmpeg.avformat_alloc_output_context2(&outFormatContext, null, null, outFile) < 0)
                {
                    throw new ConvertMediaException("Failed to allocate output context");
                }

                var outStream = ffmpeg.avformat_new_stream(outFormatContext, null);

                if (outStream == null)
                    throw new ConvertMediaException("Could not found output stream.");

                ffmpeg.avcodec_parameters_copy(outStream->codecpar, inStream->codecpar);
                outStream->time_base = new AVRational() {num = 1, den = fps};
                outStream->codecpar->codec_tag = 0;

                if ((outFormatContext->oformat->flags & ffmpeg.AVFMT_NOFILE) == 0)
                {
                    if (ffmpeg.avio_open(&outFormatContext->pb, outFile, ffmpeg.AVIO_FLAG_WRITE) < 0)
                    {
                        throw new ConvertMediaException("Failed to open output file.");
                    }
                }

                if (ffmpeg.avformat_write_header(outFormatContext, null) < 0)
                {
                    throw new ConvertMediaException("Failed to write header to output file");
                }

                var ts = 0L;
                while (true)
                {
                    AVPacket videoPkt;
                    if (ffmpeg.av_read_frame(inFormatContext, &videoPkt) < 0)
                    {
                        break;
                    }
                    videoPkt.stream_index = outStream->index;
                    videoPkt.pts = ts;
                    videoPkt.dts = ts;
                    videoPkt.duration = ffmpeg.av_rescale_q(videoPkt.duration, inStream->time_base,
                        outStream->time_base);
                    ts += videoPkt.duration;
                    videoPkt.pos = -1;

                    if (ffmpeg.av_interleaved_write_frame(outFormatContext, &videoPkt) < 0)
                    {
                        ffmpeg.av_packet_unref(&videoPkt);
                        throw new ConvertMediaException("Failed to mux packet");
                    }
                    ffmpeg.av_packet_unref(&videoPkt);
                }

                ffmpeg.av_write_trailer(outFormatContext);

                // Free
                if (inFormatContext != null)
                {
                    ffmpeg.avformat_close_input(&inFormatContext);
                }
                if (outFormatContext != null && (outFormatContext->oformat->flags & ffmpeg.AVFMT_NOFILE) == 0)
                {
                    ffmpeg.avio_closep(&outFormatContext->pb);
                }
                if (outFormatContext != null)
                {
                    ffmpeg.avformat_free_context(outFormatContext);
                }

                return File.ReadAllBytes(outFile);
            }
        }
    }
}