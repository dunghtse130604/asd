﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace JB2.MainProc
{
    public class InteropHelper
    {
        protected InteropHelper() { }

        public const string LdLibraryPath = "LD_LIBRARY_PATH";

        public static void RegisterLibrariesSearchPath(string path)
        {
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                    SetDllDirectory(path);
                    break;
                case PlatformID.Unix:
                case PlatformID.MacOSX:
                    string currentValue = Environment.GetEnvironmentVariable(LdLibraryPath);
                    if (!string.IsNullOrWhiteSpace(currentValue) && !currentValue.Contains(path))
                    {
                        string newValue = currentValue + Path.PathSeparator + path;
                        Environment.SetEnvironmentVariable(LdLibraryPath, newValue);
                    }
                    break;
            }
        }

        [DllImport("kernel32", SetLastError = true)]
        private static extern bool SetDllDirectory(string lpPathName);
    }
}