﻿using JB2.Core;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace JB2.MainProc
{
    public class EventDataProcessing
    {
        protected EventDataProcessing() { }
        private const int DefaultFps = 10;

        public static async Task Run(CloudQueueMessage queueMessage,
            DocumentClient dbClient,
            CloudBlobContainer iothubBlobContainer,
            CloudBlobContainer sensorOutputBlob,
            CloudBlobContainer videoOutputBlob,
            CloudBlobContainer sensorDataContainer,
            IAsyncCollector<EventData> tramissionEventHub,
            ExecutionContext context,
            TraceWriter writer, ILogger logger)
        {
            var timeList = new List<long>();
            var fps = DefaultFps;

            //1. Receive a message from EventProcessing queue
            var jsonEventQueue = JsonConvert.DeserializeObject<EventProcessingMessage>(queueMessage.AsString);
            var deviceId = jsonEventQueue.deviceId;
            var fromTime = jsonEventQueue.fromTime;
            var toTime = jsonEventQueue.toTime;
            var requestSource = jsonEventQueue.requestSource;
            logger = logger.AddDeviceIdProp(deviceId);
            logger.Info($"Generate eventData: {queueMessage.AsString}");

            //Get data of MDT
            //2. Get mdt documents where [fromTime <= mdt.time
            //&& mdt.time <= toTime] from TelemetryData collection
            var listMdtTarget = dbClient.TelemetryQuery<Mdt>(deviceId)
                .Where(mdt => mdt.messageType == MessageType.Mdt && fromTime <= mdt.data.time && mdt.data.time <= toTime)
                .ToList()
                .OrderBy(mdt => mdt.data.time)
                .ToList();

            //3. Get h264 stream files of the mdt documents from iothub container
            //&&
            //5. Make MP4 file from h264 stream files and upload it into video container
            var h264BlobDirectory = iothubBlobContainer.GetDirectoryReference(deviceId);
            var videoBlobPath = string.Empty;
            var videoDuration = 0;
            using (var mergeVideoStream = new MemoryStream())
            {
                foreach (var mdtTarget in listMdtTarget)
                {
                    timeList.Add(mdtTarget.data.time);
                    var timeInNewFormat = mdtTarget.data.time.ToDatetimeString(TelemetryData.DateFormat);
                    var streamBlob = h264BlobDirectory.GetBlockBlobReference(timeInNewFormat + "_h264");
                    if (await streamBlob.ExistsAsync())
                    {
                        // CR 1019: If there is no MDT data in TCU, all fields are 0
                        if (mdtTarget.data.video.fps != 0)
                        {
                            fps = mdtTarget.data.video.fps;
                        }
                        var bytes = await streamBlob.DownloadByteArrayAsync();
                        // Merge all h264 in to one file
                        mergeVideoStream.Write(bytes, 0, bytes.Length);
                        videoDuration++;
                    }
                    else
                    {
                        logger.Debug("Get h264 files - NOT FOUND!");
                    }
                }

                // store it into video container
                if (mergeVideoStream.Length > 0)
                {
                    var converter = H264Converter.GetInstance(context.FunctionDirectory);
                    var combinedMp4Bytes = converter.ConvertToVideo(mergeVideoStream.ToArray(), fps);

                    //Path of video is <time:YYYYMMDD_hhmmss>_<duration:000>.mp4
                    var blockBlob = videoOutputBlob.GetBlockBlobReference(
                        $"{deviceId}/{timeList[0].ToDatetimeString(TelemetryData.DateFormat)}_{videoDuration:000}.mp4");
                    await blockBlob.UploadByteArray(combinedMp4Bytes);
                    videoBlobPath = blockBlob.GetBlobPath();
                    logger.Debug($"Converted and store h264 files - Uploaded successful - {videoBlobPath}");
                }
            }

            //4. Get data for the target range from SensorData collection
            var sensorList = new List<SensorCsvData>();
            for (var idx = fromTime; idx < toTime + 1; idx++)
            {
                var csvSensorBlob = sensorDataContainer.GetBlockBlobReference(
                    deviceId + "/" + idx.ToDatetimeString(TelemetryData.DateFormat) + ".csv");
                if (await csvSensorBlob.ExistsAsync())
                {
                    var csvString = await csvSensorBlob.DownloadTextAsync();
                    var sensorData = CsvUtils.ParseCsvString<SensorCsvData>(csvString);
                    if (sensorData != null)
                    {
                        sensorList.AddRange(sensorData);
                    }
                }
                else
                {
                    logger.Debug($"SensorData is not existed blob: {idx}.csv");
                }
            }

            //5.1 Upload sensor csv file into sensor container
            var sensorBlockBlobPath = string.Empty;
            if (sensorList.Any())
            {
                var csvBytes = CsvUtils.ConvertCsvBytes(sensorList);

                //Upload to blob despite of being existed or not
                if (csvBytes != null)
                {
                    var blockBlob = sensorOutputBlob.GetBlockBlobReference(
                        $"{deviceId}/{timeList[0].ToDatetimeString(TelemetryData.DateFormat)}_{Math.Round((sensorList.Count - 1) / 100d):000}.csv");
                    await blockBlob.UploadByteArray(csvBytes);
                    sensorBlockBlobPath = blockBlob.GetBlobPath();
                }
            }
            else
            {
                logger.Warning($"No sensor data: {queueMessage.AsString}");
            }

            //Get data of EVENT
            var eventTargetList = dbClient.TelemetryQuery<Event>(deviceId)
                .Where(e => e.id == requestSource)
                .ToList();

            if (eventTargetList.Any())
            {
                var eventTarget = eventTargetList[0];

                // EventData creator
                var eventData = new EventData_()
                {
                    time = eventTarget.data.time,
                    gps = eventTarget.data.gps,
                    flags = eventTarget.data.flags,
                    strength = eventTarget.data.strength,
                    photo = eventTarget.data.photo,
                    data = new Data
                    {
                        duration = (int)(toTime - fromTime + 1),
                        time = fromTime,
                        video = videoBlobPath,
                        sensor = sensorBlockBlobPath
                    }
                };

                //6. Send the eventData document as "eventData" data to EventHub
                var jsonData = new
                {
                    eventTarget.deviceId,
                    eventTarget.sdId,
                    eventTarget.data.time,
                    eventTarget.data.gps,
                    eventTarget.data.flags,
                    eventTarget.data.strength,
                    eventTarget.data.photo,
                    eventData.data
                };
                var transmissionData = TransmissionData.Create(jsonData.ToJsonBytes(),
                    eventTarget.deviceId, MessageType.EventData, eventTarget.sdId, eventTarget.data.time);
                await tramissionEventHub.PushEventDataAsync(transmissionData);

                // Upsert the eventData document into TelemetryData collection
                await dbClient.UpsertTelemetryAsync(
                    eventTarget.deviceId,
                    eventTarget.sdId,
                    UnixTime.Now,
                    UnixTime.Now,
                    eventData);
            }
            else
            {
                logger.Warning($"No event telemetry data: {queueMessage.AsString}");
            }
        }
    }
}