﻿using IniParser.Model;
using IniParser.Parser;
using JB2.Core;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace JB2.MainProc
{
    public class ConfigurationApi
    {
        public static async Task<HttpResponseMessage> Run(HttpRequestMessage req, string deviceId, ILogger logger, CloudTable deviceInfo,
                            DocumentClient dbClient, CloudTable settingInfo, ExecutionContext context)
        {
            // DeviceTwin url property stores parameters with uri encoding, but GetQueryNameValuePairs returns parameters with uri decoding
            var parameters = req.GetQueryNameValuePairs().ToDictionary(kv => kv.Key, kv => kv.Value);
            logger = logger.AddDeviceIdProp(deviceId);
            var code = parameters.FirstOrDefault(q => q.Key == "code").Value;
            if (!SecurityUtils.GenerateAuthenKey(AppEnvironment.IoTHubConnectionString, deviceId).Equals(code))
            {
                logger.Warning("Authentication: Code is invalid");
                return new HttpResponseMessage(HttpStatusCode.Unauthorized);
            }

            var type = parameters.FirstOrDefault(q => q.Key == "type").Value;
            var body = await req.Content.ReadAsStringAsync();
            StringContent content;
            switch (type)
            {
                case "initialize":
                    logger.Info($"Get initial settings: body={body}");
                    content = await Initialize(deviceId, deviceInfo, dbClient, settingInfo, body, logger);
                    break;
                case "setting":
                    logger.Info($"Get setting.ini: body={body}");
                    content = Setting(deviceId, body, settingInfo, context);
                    break;
                default:
                    logger.Warning($"Type is invalid: {type}");
                    return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = content
            };
        }

        private static async Task<StringContent> Initialize(string deviceId, CloudTable deviceInfo,
            DocumentClient dbClient, CloudTable settingInfo, string body, ILogger logger)
        {
            //If 'reset' parameter in request body == 1. Execute VPC reset
            if (!string.IsNullOrEmpty(body))
            {
                try
                {
                    var reset = JsonConvert.DeserializeObject<dynamic>(body).reset.Value;
                    if (reset == 1)
                    {
                        var settingInfoItems = settingInfo.QueryDeviceEntities<SettingInfo>(deviceId);
                        await HandleResetVpc(settingInfoItems, deviceInfo, settingInfo, deviceId);
                    }
                }
                catch (JsonException ex)
                {
                    logger.Warning("Invalid body request: " + ex.Message);
                }
            }

            var lastDriveTime = (await deviceInfo.RetrieveEntityAsync(
                                    new DeviceInfo<long>(deviceId, DeviceInfoSchema.LastReceivedDriveTime)))?.value ?? 0;
            var lastSysLog = await GetLogIndex(deviceInfo, dbClient, deviceId, DeviceInfoSchema.LastSystemLogIndex);
            var lastErrLog = await GetLogIndex(deviceInfo, dbClient, deviceId, DeviceInfoSchema.LastErrorLogIndex);

            // If FCW Enabled and setting.ADAS.VP_X not exists (it means VPC is not completed or VPC reset), enable sending VPC message
            var fcwEnable = (await settingInfo.RetrieveEntityAsync(
                                new SettingInfo(deviceId, SettingSchema.ADAS, SettingSchema.FCW_ENABLE)))?.value;
            var vpXEnt = await settingInfo.RetrieveEntityAsync(new SettingInfo(deviceId, SettingSchema.ADAS, SettingSchema.VP_X));
            var vpcInterval = fcwEnable == "1" && vpXEnt == null ? ExternalConfig.VpcInterval : -1;
            var vpcSpeed = ExternalConfig.VpcSpeed;
            var vpcMaxNum = ExternalConfig.VpcMaxNum;
            var eventPhoto = 0;
            logger.Debug($"deviceId={deviceId},lastDriveTime={lastDriveTime}, lastSysLog={lastSysLog}, lastErrorLog={lastErrLog}, vpcInterval={vpcInterval}");

            var initDataJson = JsonConvert.SerializeObject(new
            {
                lastDriveTime,
                lastSysLog,
                lastErrLog,
                vpcInterval,
                vpcSpeed,
                vpcMaxNum,
                eventPhoto
            });
            return new StringContent(initDataJson, Encoding.UTF8, "application/json");
        }

        private static StringContent Setting(string deviceId, string uploadedSettingIni,
            CloudTable settingInfo, ExecutionContext context)
        {
            var iniDataParser = new IniDataParser()
            {
                Configuration =
                {
                    SkipInvalidLines = true ,
                    AllowDuplicateKeys = true,
                    AllowDuplicateSections = true
                }
            };

            // setting.ini edited by FW includes \r\n code, so replace it to \n before parsing.
            uploadedSettingIni = uploadedSettingIni.Replace("\r\n", "\n");
            var curIniData = iniDataParser.Parse(uploadedSettingIni);

            var newIniData = new IniData
            {
                Configuration =
                {
                    AssigmentSpacer = "",
                    NewLineStr = "\n"
                }
            };

            // 1.Keep the particular sections from the uploaded ini data
            foreach (var section in curIniData.Sections)
            {
                if (ExternalConfig.DebugSections.Contains(section.SectionName))
                {
                    newIniData.Sections.Add(section);
                }
            }

            // 2. Add common setting.ini properties to the above ini data.
            var commonSettingIni = File.ReadAllText(Path.Combine(context.FunctionDirectory, "setting.ini"));
            newIniData.Merge(iniDataParser.Parse(commonSettingIni));

            // 3. Add device setting properties (queried from SettingInfo table) to the above ini data
            var settingInfoItems = settingInfo.QueryDeviceEntities<SettingInfo>(deviceId);
            var settingProperties = new IniData();
            foreach (var setting in settingInfoItems)
            {
                settingProperties.Sections.AddSection(setting.section);
                settingProperties[setting.section].AddKey(setting.key, setting.value);
            }
            newIniData.Merge(settingProperties);

            // [Workaround] Empty value is handled as nothing (FW clear vpc settings with empty value, so if empty value, remove the key)
            foreach (var section in curIniData.Sections)
            {
                foreach (var key in section.Keys.ToArray())
                {
                    if (string.IsNullOrEmpty(key.Value))
                    {
                        section.Keys.RemoveKey(key.KeyName);
                    }
                }
            }

            // If setting ini is not changed, return with empty.
            var content = !IsSameIni(curIniData, newIniData) ? newIniData.ToString() : "";

            return new StringContent(content, Encoding.UTF8, "text/plain");
        }

        private static bool IsSameIni(IniData curIniData, IniData newIniData)
        {
            // Compare number of sections
            if (curIniData.Sections.Count != newIniData.Sections.Count)
            {
                return false;
            }
            foreach (var curIniSection in curIniData.Sections)
            {
                var newIniKeys = newIniData.Sections[curIniSection.SectionName];
                // Compare number of keys
                if (curIniSection.Keys.Count != newIniKeys?.Count)
                {
                    return false;
                }
                // Compare values in each key
                foreach (var curIniKey in curIniSection.Keys)
                {
                    if (curIniKey.Value != newIniKeys?[curIniKey.KeyName])
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public static async Task<long> GetLogIndex(CloudTable deviceInfo, DocumentClient dbClient, string deviceId, string rowKey)
        {
            // Get index of the latest log index from DeviceInfo. If there is log index entity, return -1.
            var lastLogIndexEntity = await deviceInfo.RetrieveEntityAsync(new DeviceInfo<long>(deviceId, rowKey));
            return lastLogIndexEntity != null ? lastLogIndexEntity.value : -1;
        }

        public static async Task HandleResetVpc(IList<SettingInfo> settingInfoItems, CloudTable deviceInfoTable, CloudTable settingInfoTable, string deviceId)
        {
            // Update lastResetTime Item
            await deviceInfoTable.UpsertEntityAsync(
                new DeviceInfo<long>(deviceId, DeviceInfoSchema.LastResetTime, UnixTime.Now));

            // Remove ADAS setting from SettingInfo table.
            var operations = new TableBatchOperation();
            settingInfoItems
                .Where(d => d.section == SettingSchema.ADAS && SettingSchema.VpcSettings.Contains(d.key))
                .ToList()
                .ForEach(item => { operations.Delete(item); });
            if (operations.Any())
            {
                await settingInfoTable.ExecuteBatchAsync(operations);
            }
        }
    }
}