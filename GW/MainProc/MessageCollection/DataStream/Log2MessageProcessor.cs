﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using JB2.Core;
using Newtonsoft.Json;

namespace JB2.MainProc
{
    public class Log2MessageProcessor : BaseMessageProcessor
    {
        protected override async Task Run()
        {
            Logger.Debug($"Collect log message: {JsonContent}");

            if (string.IsNullOrEmpty(JsonContent))
            {
                Logger.Warning("Log content have no Data");
                return;
            }

            var logSourceArray = JsonContent.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None);

            var maxSystemLogIndex = -1;
            var maxErrorLogIndex = -1;
            foreach (var logSource in logSourceArray)
            {
                if (string.IsNullOrEmpty(logSource))
                {
                    continue;
                }

                var log = LogMessageFactory.Create(logSource);
                if (log == null)
                {
                    Logger.Warning($"Unknown log. LogSource={logSource}");
                    continue;
                }

                // Save each log message into database and find the max index of error log and system log.
                if (!ExternalConfig.IgnoredLogs.Contains(log.type))
                {
                    await DbClient.UpsertTelemetryAsync(DeviceId, SdId, SentTime, ReceivedTime, log);
                }

                if (log.type >= 0 && log.index > maxSystemLogIndex)
                {
                    maxSystemLogIndex = log.index;
                }

                if (log.type < 0 && log.index > maxErrorLogIndex)
                {
                    maxErrorLogIndex = log.index;
                }
            }

            if (maxSystemLogIndex >= 0)
            {
                await DeviceInfoTable.UpsertEntityAsync(
                    new DeviceInfo<long>(DeviceId, DeviceInfoSchema.LastSystemLogIndex, maxSystemLogIndex));
            }

            if (maxErrorLogIndex >= 0)
            {
                await DeviceInfoTable.UpsertEntityAsync(
                    new DeviceInfo<long>(DeviceId, DeviceInfoSchema.LastErrorLogIndex, maxErrorLogIndex));
            }
        }
    }
}