﻿using JB2.Core;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace JB2.MainProc
{
    public class LogMessageProcessor : BaseMessageProcessor
    {
        protected override async Task Run()
        {
            Logger.Debug($"Collect log message: {JsonContent}");

            // Get mesage content from application properties
            var streamTxt = JsonContent;
            if (string.IsNullOrEmpty(streamTxt))
            {
                Logger.Warning("Log content have no Data");
                return;
            }

            var listArray = streamTxt.Split(new[] { "\r\n", "\n" }, StringSplitOptions.None);
            var listLog = listArray.Select(JsonConvert.DeserializeObject<Log>).ToList();

            if (listLog.Any())
            {
                foreach (var logItem in listLog)
                {
                    if (!ExternalConfig.IgnoredLogs.Contains(logItem.type))
                    {
                        await DbClient.UpsertTelemetryAsync(DeviceId, SdId, SentTime, ReceivedTime, logItem);
                    }
                    //1305.Store index of log to avoid query Document DB in MessageSender.HandleInitialization()
                    await DeviceInfoTable.UpsertEntityAsync(new DeviceInfo<long>(
                        DeviceId,
                        logItem.error == 0 ? DeviceInfoSchema.LastSystemLogIndex
                                           : DeviceInfoSchema.LastErrorLogIndex,
                        logItem.index));
                }
            }
        }
    }
}