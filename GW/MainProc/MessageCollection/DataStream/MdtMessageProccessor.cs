﻿using JB2.Core;
using Microsoft.WindowsAzure.Storage;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JB2.MainProc
{
    public class MdtMessageProccessor : BaseMessageProcessor
    {
        protected override async Task Run()
        {
            Logger.Info($"Collect mdt message: {JsonContent}");

            var mdtMessage = JsonConvert.DeserializeObject<Mdt>(JsonContent);
            // [Compatible] Old FW send extra data with one space if no sensor data, so check the data length.
            if (DataContent != null && DataContent.Length > 1)
            {
                var sensorTsvStr = Encoding.UTF8.GetString(GzipUtils.Decompress(DataContent));
                // Upload sensor data to blob
                var csvBlob = SensorContainer.GetBlockBlobReference(
                    $"{DeviceId}/{mdtMessage.time.ToDatetimeString(TelemetryData.DateFormat)}.csv");
                await csvBlob.UploadTextAsync(sensorTsvStr);
            }
            else
            {
                Logger.Info("No sensor data");
            }

            // Only upload media content if TCU send media in body
            if (MediaContent != null && !Message.HasBlob())
            {
                // Upload h264 media content
                var mediaBlob = IoTHubBlobContainer.GetBlockBlobReference(
                    $"{DeviceId}/{mdtMessage.time.ToDatetimeString(TelemetryData.DateFormat)}_h264");
                await mediaBlob.UploadByteArray(MediaContent);
            }

            // Update mdt record
            await DbClient.UpsertTelemetryAsync(DeviceId, SdId, SentTime, ReceivedTime, mdtMessage);

            // Get all entry for this deviceId from DataRequest table storage (filter data purpose)
            var dataRequestItems = DataRequestTable.QueryDeviceEntities<DataRequest>(DeviceId);

            var mdtTelemetryList = DbClient.TelemetryQuery<Mdt>(DeviceId)
                .Where(mdt => mdt.receivedTime == 0 && mdt.messageType == MessageType.Mdt)
                .ToList();

            foreach (var requestItem in dataRequestItems)
            {
                try
                {
                    var mdtCnt = mdtTelemetryList.Count(x => x.data.time >= requestItem.fromTime &&
                                                             x.data.time <= requestItem.toTime);
                    if (mdtCnt != 0)
                    {
                        Logger.Debug($"[Continue] Request data fromTime toTime is not 0 record: {mdtCnt} records");
                        continue;
                    }
                    // Remove the record from DataRequest table
                    //requestItem.ETag = "*";

                    // Execute delete
                    await DataRequestTable.DeleteEntityAsync(requestItem);

                    // Add message to EventProcessing queue
                    var eventProcessingMessage = new EventProcessingMessage
                    {
                        deviceId = DeviceId,
                        fromTime = requestItem.fromTime,
                        toTime = requestItem.toTime,
                        requestSource = requestItem.requestSource
                    };

                    await EventDataQueueItems.PushQueueMessageAsync(eventProcessingMessage);
                    Logger.Debug($"[{DeviceId}] Processed item: {requestItem.fromTime} - {requestItem.toTime}");
                }
                catch (StorageException e)
                {
                    // Only ignore if delete non-found record
                    if (e.RequestInformation.HttpStatusCode != 404)
                    {
                        throw;
                    }
                }
            }
        }
    }
}