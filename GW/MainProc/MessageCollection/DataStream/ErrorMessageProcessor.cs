﻿using JB2.Core;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace JB2.MainProc
{
    public class ErrorMessageProcessor : BaseMessageProcessor
    {
        protected override async Task Run()
        {
            Logger.Info($"Collect error message: {JsonContent}");

            // Get mesage content from application properties
            var errorData = JsonConvert.DeserializeObject<Error>(JsonContent);

            // Compatibile code: handle 'error' field as 'status' field ('error' filed name will be removed in future)
            var errorJson = JsonConvert.DeserializeObject<dynamic>(JsonContent);
            if (errorJson.error != null)
            {
                errorData.status = errorJson.error;
            }

            // Create output item
            // - Telemetry Error document
            await DbClient.UpsertTelemetryAsync(DeviceId, SdId, SentTime, ReceivedTime, errorData);

            // - Tramission event hub data
            var jsonData = new
            {
                deviceId = DeviceId,
                sdId = SdId,
                errorData.time,
                errorData.status
            };
            var transmissionData = TransmissionData.Create(jsonData.ToJsonBytes(), DeviceId, MessageType.Error, SdId, errorData.time);
            await TransmissionEventHub.PushEventDataAsync(transmissionData);
        }
    }
}