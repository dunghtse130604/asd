using JB2.Core;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JB2.MainProc
{
    public class StartupMessageProcessor : BaseMessageProcessor
    {
        protected override async Task Run()
        {
            Logger.Info($"Collect startup message: {JsonContent}");

            // 1.Get mesage content from application properties
            var startupData = JsonConvert.DeserializeObject<Startup>(JsonContent);

            // Get all entry for this deviceId from DeviceInfo table storage (filter data purpose)
            var settingInfoItems = SettingInfoTable.QueryDeviceEntities<SettingInfo>(DeviceId);

            // [Compatible code] Steps for old version
            if (Message.IsOldVersion())
            {
                // If VPC Reset in the data is 1 and device use old firmare -> Reset VPC
                if (startupData.vpcReset == 1)
                {
                    await ConfigurationApi.HandleResetVpc(settingInfoItems, DeviceInfoTable, SettingInfoTable, DeviceId);
                }

                // Send OnInitialization message to DeviceNotification queue
                var notificationMsg = new DeviceNotificationMessage
                {
                    deviceId = DeviceId,
                    sdId = SdId,
                    actionType = DeviceNotificationMessage.ActionType.OnInitialization,
                    parameter = null
                };
                await DeviceNotificationQueue.PushQueueMessageAsync(notificationMsg);
            }
            else
            {
                // New firmware behavior: Query all availalble mdt and push OnMdtRequest to queue
                var mdtRequestList = DbClient.TelemetryQuery<Mdt>(DeviceId)
                    .Where(s => s.receivedTime == 0 && s.messageType == MessageType.Mdt).ToList();
                if (mdtRequestList.Any())
                {
                    var mdtRequest = string.Join(",", mdtRequestList.Select(x => x.data.time));
                    var notificationMsg = new DeviceNotificationMessage
                    {
                        deviceId = DeviceId,
                        sdId = SdId,
                        actionType = DeviceNotificationMessage.ActionType.OnMdtRequest,
                        parameter = mdtRequest
                    };
                    await DeviceNotificationQueue.PushQueueMessageAsync(notificationMsg);
                }
            }

            // 5.If [newFw entity exists in DeviceInfo table] and [value of the entity == fw of the message],
            var newFwEntity = await DeviceInfoTable.RetrieveEntityAsync(new DeviceInfo<string>(DeviceId, DeviceInfoSchema.NewFw));
            if (newFwEntity != null)
            {
                if (newFwEntity.value != startupData.fw)
                {
                    // Send OnFota with the newFw value as parameter
                    var onFotaMsg = new DeviceNotificationMessage
                    {
                        deviceId = DeviceId,
                        sdId = SdId,
                        actionType = DeviceNotificationMessage.ActionType.OnFota,
                        parameter = newFwEntity.value
                    };

                    await DeviceNotificationQueue.PushQueueMessageAsync(onFotaMsg);
                    Logger.Debug("Added OnFota to DeviceNotification queue");
                }
                else
                {
                    // Remove the newFw entity
                    await DeviceInfoTable.DeleteEntityAsync(newFwEntity);
                }
            }

            // Calculate for fcw fields
            startupData.fcw = new Fcw
            {
                calibration = CalculateCalibration(settingInfoItems),
                progress = await CalculateFcwProgress()
            };

            // 8.Upsert startup document into TelemetryData collection
            await DbClient.UpsertTelemetryAsync(DeviceId, SdId, SentTime, ReceivedTime, startupData);

            // 9.Send startup document into transmission EventHub
            var jsonData = new
            {
                deviceId = DeviceId,
                sdId = SdId,
                startupData.time,
                startupData.gps,
                startupData.fw,
                startupData.phone,
                startupData.sensor,
                startupData.fcw
            };
            var transmissionData = TransmissionData.Create(jsonData.ToJsonBytes(), DeviceId, MessageType.Startup, SdId, startupData.time);
            await TransmissionEventHub.PushEventDataAsync(transmissionData);

            // 10. Upsert entity into LatestInfo table
            var latestData = new LatestStartupInfo(DeviceId)
            {
                sdId = SdId,
                fw = startupData.fw,
                phone = startupData.phone,
                calibration = startupData.sensor.calibration,
                angle = startupData.sensor.angle
            };

            await LatestInfoTable.UpsertEntityAsync(latestData);
        }

        private int CalculateCalibration(IList<SettingInfo> settingInfoItems)
        {
            // Check existance of setting.ADAS.VP_X -> set value for fcw.calibration
            return settingInfoItems.Any(d => d.section == "ADAS" && d.key == "VP_X") ? 1 : 0;
        }

        private async Task<int> CalculateFcwProgress()
        {
            var lastResetTime = (await DeviceInfoTable.RetrieveEntityAsync(
                new DeviceInfo<long>(DeviceId, DeviceInfoSchema.LastResetTime)))?.value ?? 0;

            // count of vpc documents of [time > lastResetTime of DeviceInfo and result.status > 0] from TelemetryData
            return DbClient
                .TelemetryQuery<Vpc>(DeviceId)
                .Where(telemetry => (telemetry.data.result.status == 1 || telemetry.data.result.status == 2)
                                    && telemetry.data.time > lastResetTime)
                .AsEnumerable()
                .Count();
        }
    }
}