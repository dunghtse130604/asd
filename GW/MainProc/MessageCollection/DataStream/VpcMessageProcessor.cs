﻿using JB2.Core;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Security;
using System.Threading.Tasks;
using TosVPC.CLI;

namespace JB2.MainProc
{
    public class VpcMessageProcessor : BaseMessageProcessor
    {
        // Default vpc setting
        private static readonly VPC_INIT_PARAM VpcInitParam = new VPC_INIT_PARAM
        {
            vp_y_samples = 8,
            vp_thresh_hold = 8,
            count_thresh_hold = 40,
            block_size_be = 32,
            block_slide_size = 8,
            be_thresh_hold = 620,
            be_y_samples = 8
        };

        protected override async Task Run()
        {
            Logger.Info($"Collect vpc message: {JsonContent}");

            var vpcData = JsonConvert.DeserializeObject<Vpc>(JsonContent);

            // Get all entry for this deviceId from DeviceInfo table storage (filter data purpose)
            var lastResetTime = (await DeviceInfoTable.RetrieveEntityAsync(
                new DeviceInfo<long>(DeviceId, DeviceInfoSchema.LastResetTime)))?.value ?? 0;
            Logger.Debug($"Got lastResetTime from deviceInfo: {lastResetTime}");

            // Get previous vp data
            var preVpData = DbClient
                .TelemetryQuery<Vpc>(DeviceId)
                .Where(telemetry => telemetry.data.result.status == 1 && telemetry.data.time >= lastResetTime)
                .ToList()
                .OrderBy(telemetry => telemetry.data.time)
                .Select(telemetry => telemetry.data.result);

            // 4. Convert from the iFrame data to YUV image, and put the image and the previous results into VPC lib
            var converter = H264Converter.GetInstance(FunctionDirectory);
            var yuvBytes = converter.ConvertToImage(MediaContent, ImageType.YUV);

            // Convert IFrame data to JPEG image and store it to photo container.
            var photoBytes = converter.ConvertToImage(MediaContent, ImageType.JPEG);

            var photoBlob = PhotoBlobContainer.GetBlockBlobReference(
                $"{DeviceId}/{vpcData.time.ToDatetimeString(TelemetryData.DateFormat)}.jpg");
            await photoBlob.UploadByteArray(photoBytes);
            vpcData.photo = photoBlob.GetBlobPath();

            // Get ADAS_CAR_HEIGHT from SettingInfo table
            var carHeight = (await SettingInfoTable.RetrieveEntityAsync(
                new SettingInfo(DeviceId, SettingSchema.ADAS, SettingSchema.CAR_HEIGHT)))?.value;

            if (string.IsNullOrEmpty(carHeight))
            {
                Logger.Warning($"Setting ADAS_CAR_HEIGHT is empty.");
                return;
            }

            var result = CalculateByDll(int.Parse(carHeight), preVpData, yuvBytes);
            vpcData.result = result;

            // 5. Upsert vpc document with the result into TelemetryData collection
            await DbClient.UpsertTelemetryAsync(DeviceId, SdId, SentTime, ReceivedTime, vpcData);

            if (result.status == 2)
            {
                Logger.Debug($"[{DeviceId}] VPC completed");

                // 6. Upsert the following records into DeviceInfo
                var operations = new TableBatchOperation();
                operations.InsertOrReplace(new SettingInfo(DeviceId, SettingSchema.ADAS, SettingSchema.VP_X, result.vp_x.ToString()));
                operations.InsertOrReplace(new SettingInfo(DeviceId, SettingSchema.ADAS, SettingSchema.VP_Y, result.vp_y.ToString()));
                operations.InsertOrReplace(new SettingInfo(DeviceId, SettingSchema.ADAS, SettingSchema.BE_P, result.be_p.ToString()));
                operations.InsertOrReplace(new SettingInfo(DeviceId, SettingSchema.ADAS, SettingSchema.HD_VP_X, result.hd_vp_x.ToString()));
                operations.InsertOrReplace(new SettingInfo(DeviceId, SettingSchema.ADAS, SettingSchema.HD_VP_Y, result.hd_vp_y.ToString()));
                operations.InsertOrReplace(new SettingInfo(DeviceId, SettingSchema.ADAS, SettingSchema.HD_BE_P, result.hd_be_p.ToString()));
                await SettingInfoTable.ExecuteBatchAsync(operations);

                // 7.Binding out DeviceNotificationQueue OnSetting message queue
                var notificationMsg = new DeviceNotificationMessage
                {
                    deviceId = DeviceId,
                    sdId = SdId,
                    actionType = DeviceNotificationMessage.ActionType.OnSetting,
                    parameter = null
                };

                await DeviceNotificationQueue.PushQueueMessageAsync(notificationMsg);
            }
        }

        [HandleProcessCorruptedStateExceptions]
        [SecurityCritical]
        private Result CalculateByDll(int carHeight, IEnumerable<Result> preResults, byte[] imgData)
        {
            const int hdWidth = 1280;
            const int hdHeight = 720;
            using (var vpcClient = new TVPC())
            {
                var result = new Result()
                {
                    status = -1
                };
                
                var status = vpcClient.TVPC_InitializeWithSetting(VpcInitParam);
                if (status != (int)TVPC_STATUS.TVPC_SUCCESSFUL)
                {
                    Logger.Warning($"Error return from InitWithSetting: code={status}");
                    return result;
                }

                try
                {
                    var data = preResults.Select(r => new VP_Data
                    {
                        VP_X = r.vp_x,
                        VP_Y = r.vp_y,
                        BE_P = r.be_p,
                        HD_VP_X = r.hd_vp_x,
                        HD_VP_Y = r.hd_vp_y,
                        HD_BE_P = r.hd_be_p
                    })
                    .ToArray();
                    var calc = new VP_Data();
                    var fixedVpc = new VP_Data();
                    status = vpcClient.TVPC_SetImage2(data, data.Length, imgData, hdWidth, hdHeight, carHeight, ref calc, ref fixedVpc);

                    switch (status)
                    {
                        case TVPC_STATUS.TVPC_SUCCESSFUL:
                            result.status = 2;
                            calc = fixedVpc;
                            break;

                        case TVPC_STATUS.TVPC_NEED_MORE_DATA:
                            // If all result values are 0, it means invalid status (0)
                            result.status = (calc.VP_X == 0 && calc.VP_Y == 0 && calc.BE_P == 0) ? 0 : 1;
                            break;

                        default:
                            Logger.Warning($"Error return from TVPC_SetImage2: code={status}");
                            break;
                    }

                    result.vp_x = calc.VP_X;
                    result.vp_y = calc.VP_Y;
                    result.be_p = calc.BE_P;
                    result.hd_vp_x = calc.HD_VP_X;
                    result.hd_vp_y = calc.HD_VP_Y;
                    result.hd_be_p = calc.HD_BE_P;
                }
                catch (Exception e)
                {
                    Logger.Exception(e);
                }
                return result;
            }
        }
    }
}
