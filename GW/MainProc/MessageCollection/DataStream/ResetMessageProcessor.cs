﻿using JB2.Core;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace JB2.MainProc
{
    public class ResetMessageProcessor : BaseMessageProcessor
    {
        protected override async Task Run()
        {
            Logger.Info($"Collect reset message: {JsonContent}");

            // Get mesage content from application properties
            var resetData = JsonConvert.DeserializeObject<Reset>(JsonContent);

            if (MediaContent != null)
            {
                // Convert iFrame -> Jpeg image and store to photo container.
                var jpgBytes = H264Converter.GetInstance(FunctionDirectory)
                    .ConvertToImage(MediaContent, ImageType.JPEG);
                var photoBlob = PhotoBlobContainer.GetBlockBlobReference(
                    $"{DeviceId}/{resetData.time.ToDatetimeString(TelemetryData.DateFormat)}.jpg");
                await photoBlob.UploadByteArray(jpgBytes);
                resetData.photo = photoBlob.GetBlobPath();
            }

            // - Telemetry Reset document
            await DbClient.UpsertTelemetryAsync(DeviceId, SdId, SentTime, ReceivedTime, resetData);

            // - Tramission event hub data
            var jsonData = new
            {
                deviceId = DeviceId,
                sdId = SdId,
                resetData.time,
                resetData.gps,
                resetData.photo
            };
            var transmissionData = TransmissionData.Create(jsonData.ToJsonBytes(), DeviceId, MessageType.Reset, SdId, resetData.time);
            await TransmissionEventHub.PushEventDataAsync(transmissionData);
        }
    }
}