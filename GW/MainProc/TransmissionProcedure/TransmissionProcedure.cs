using JB2.Core;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Table;
using System.Threading.Tasks;

namespace JB2.MainProc
{
    public class TransmissionProcedure : EventHubTriggerHandler
    {
        private CloudTable inTableDeviceInfo;
        private IAsyncCollector<EventData> outEventHubMessage;

        public static async Task Run(EventData transmissionData, CloudTable inTableDeviceInfo, ILogger logger,
            IAsyncCollector<EventData> outEventHubMessage, IAsyncCollector<EventData> outEventHubPoison)
        {
            var msg = new JB2Message(transmissionData);
            if (!msg.ParseData(logger))
            {
                return;
            }
            logger = logger.AddDeviceIdProp(msg.DeviceId);
            var handler = new TransmissionProcedure
            {
                inTableDeviceInfo = inTableDeviceInfo,
                outEventHubMessage = outEventHubMessage
            };
            await handler.ExecuteAsync(msg, logger, outEventHubPoison);
        }

        protected override async Task Run()
        {
            // Get valid record from DeviceInfo table
            var valid = (await inTableDeviceInfo.RetrieveEntityAsync(
                new DeviceInfo<int>(Message.DeviceId, DeviceInfoSchema.Valid)))?.value;

            Logger.Debug($"[{Message.DeviceId}] - sdId: {Message.SdId} - valid: {valid} - messageType: {Message.MessageType}");
            if (!Message.SdId.StartsWith(ExternalConfig.KittingSdId) && (valid == 1 || Message.MessageType == MessageType.Reset))
            {
                await outEventHubMessage.PushEventDataAsync(Message.Data.Clone());
            }
        }
    }
}