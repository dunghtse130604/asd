﻿using JB2.Core;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JB2.DriveProc
{
    public class DriveCollection : EventHubTriggerHandler
    {
        static DriveCollection() { new VersionLogger(); }

        private DocumentClient dbClient;
        private CloudTable driveRowData;
        private CloudTable sentDataFrame;
        private CloudTable deviceInfo;
        private string result;

        private static readonly GlobalMetricAggerator Metric = new GlobalMetricAggerator("DriveCollection");

        public static async Task Run(EventData[] eventData, ILogger logger, CloudQueue delayQueue,
            DocumentClient dbClient, CloudTable driveRowData, CloudTable sentDataFrame, CloudTable deviceInfo, IAsyncCollector<EventData> outEventHubPoison)
        {
            var allTasks = new List<Task<string>>();
            foreach (var ev in eventData)
            {
                allTasks.Add(RetryHandling(ev, dbClient, driveRowData, sentDataFrame, deviceInfo, logger, outEventHubPoison));
            }
            await Task.WhenAll(allTasks);

            var delayMessages = new List<string>();
            foreach (var task in allTasks)
            {
                if (task.Result != null)
                {
                    delayMessages.Add(task.Result);
                }
            }

            //Find all health check device messages from original. Then add to queue with 5 seconds delay
            var hcMessages = delayMessages.FindAll(msg => msg.Contains($"\"deviceId\":\"{ExternalConfig.HealthCheckDeviceIdDrive}\""));
            if (hcMessages.Any())
            {
                var cloudQueueMessage = new CloudQueueMessage(string.Join("\n", hcMessages));
                try
                {
                    await delayQueue.AddMessageAsync(cloudQueueMessage, null, TimeSpan.FromSeconds(5), new QueueRequestOptions(), null);
                }
                catch (Exception ex)
                {
                    logger.Exception(ex);
                    logger.Error($"Failed to add message for healthcheck into queue: {cloudQueueMessage.AsString}");
                }
            }

            //Remove all health check device messsage from the original list. Then add to queue with the given second delay
            delayMessages.RemoveAll(msg => hcMessages.Contains(msg));
            if (delayMessages.Any())
            {
                var cloudQueueMessage = new CloudQueueMessage(string.Join("\n", delayMessages));
                try
                {
                    await delayQueue.AddMessageAsync(cloudQueueMessage, null, TimeSpan.FromSeconds(ExternalConfig.SendTimeout), new QueueRequestOptions(), null);
                }
                catch (Exception ex)
                {
                    logger.Exception(ex);
                    // When fails to send massage to queue, just output log because of low possibility of Storage failure
                    logger.Error($"Failed to add message into queue: {cloudQueueMessage.AsString}");
                }
            }
        }

        public static async Task<string> RetryHandling(EventData eventData, DocumentClient dbClient, CloudTable driveRowData, CloudTable sentDataFrame, CloudTable deviceInfo, ILogger logger, IAsyncCollector<EventData> outEventHubPoison)
        {
            var sw = Stopwatch.StartNew();
            var msg = new JB2Message(eventData);
            if (!msg.ParseIoTData(logger))
            {
                return null;
            }
            logger = logger.AddDeviceIdProp(msg.DeviceId);
            var handler = new DriveCollection
            {
                dbClient = dbClient,
                driveRowData = driveRowData,
                sentDataFrame = sentDataFrame,
                deviceInfo = deviceInfo
            };
            await handler.ExecuteAsync(msg, logger, outEventHubPoison);

            Metric.Add(sw);
            return handler.result;
        }

        protected override async Task Run()
        {
            Logger.Debug($"Collect drive message: {Message.JsonContent}");

            var evContent = JsonConvert.DeserializeObject<Drive>(Message.JsonContent);

            var driveRaw = GzipUtils.Decompress(Message.ExtraData);
            // Workaround: If \0 code is contained at the end of the data, trim the code (Only the initial FW includes this bug)
            if (driveRaw[driveRaw.Length - 1] == 0)
            {
                Array.Resize(ref driveRaw, driveRaw.Length - 1);
            }
            var driveCsv = Encoding.UTF8.GetString(driveRaw);

            // Find valid gps from last time
            var lastValidDrive = CsvUtils.ParseCsvString<DriveCsvData>(driveCsv)
                .LastOrDefault(s => s.GpsValid == 1);
            if (lastValidDrive != null)
            {
                evContent.lastGps = new LastGps
                {
                    time = lastValidDrive.Time,
                    latitude = lastValidDrive.GpsLatitude,
                    longitude = lastValidDrive.GpsLongitude,
                    speed = lastValidDrive.GpsSpeed,
                    heading = lastValidDrive.GpsHeading
                };
            }

            // Insert 1 mins drive data to DriveRowData Table Storage
            await driveRowData.UpsertEntityAsync(new DriveRowData(Message.DeviceId, evContent.time, driveCsv));

            // Upsert drive message to Telemetry collection
            await dbClient.UpsertTelemetryAsync(
                Message.DeviceId,
                Message.SdId,
                Message.SentTime,
                Message.ReceivedTime,
                evContent,
                ExternalConfig.DocumentTimeToLive);

            // Update last receive time to deviceInfo table
            await deviceInfo.UpsertEntityAsync(
                new DeviceInfo<long>(Message.DeviceId, DeviceInfoSchema.LastReceivedDriveTime, evContent.time + evContent.duration - 1));

            //   Upsert SentDataFrame entity
            //   Calculate start time of pack data
            var engineOnTime = evContent.engineOnTime;
            var roundedEngineOnTime = (engineOnTime - (engineOnTime % 60));
            //  Calculate rounded engineOnTime (to minute)
            //  packStartTime is calculated by algorithm
            //  Calculate number of pack
            var packNumber = (int)((evContent.time - roundedEngineOnTime) / ExternalConfig.SendInterval);
            //  Calculate start time of pack from roundedEngineOnTime
            var packStartTime = roundedEngineOnTime + packNumber * ExternalConfig.SendInterval;
            if (packStartTime < engineOnTime)
            {
                packStartTime = engineOnTime;
            }
            //  Upsert SentDataFrame entity
            var uuid = Guid.NewGuid().ToString();
            await sentDataFrame.UpsertEntityAsync(
                new SentDataFrame(Message.DeviceId, packStartTime.ToString(), uuid));

            var driveDelayMsg = new DriveProcessingMessage
            {
                deviceId = Message.DeviceId,
                sdId = Message.SdId,
                time = packStartTime,
                uuid = uuid,
                engineOnTime = engineOnTime,
            };
            result = JsonConvert.SerializeObject(driveDelayMsg);
        }
    }
}