using JB2.Core;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace JB2.DriveProc
{
    public class TriggerDetector
    {
        private static readonly GlobalMetricAggerator Metric = new GlobalMetricAggerator("TriggerDetector");

        public static async Task Run(string messages, ILogger logger, CloudQueue processingQueue,
            CloudTable inDataFrame, CloudTable inDeviceInfo)
        {
            if (string.IsNullOrEmpty(messages))
            {
                return;
            }

            var driveMessages = messages.Split('\n')
                .Where(msg => !string.IsNullOrWhiteSpace(msg));
            foreach (var msg in driveMessages)
            {
                var sw = Stopwatch.StartNew();
                try
                {
                    await ProcessSingleMessage(msg, processingQueue, inDataFrame, inDeviceInfo, logger);
                }
                finally
                {
                    Metric.Add(sw);
                }
            }
        }

        public static async Task ProcessSingleMessage(string message, CloudQueue processingQueue,
            CloudTable inDataFrame, CloudTable inDeviceInfo, ILogger logger)
        {
            var queueMessage = JsonConvert.DeserializeObject<DriveProcessingMessage>(message);

            // Get data frame from SentDataFrame table.
            var sentDataFrame = await inDataFrame.RetrieveEntityAsync(
                new SentDataFrame(queueMessage.deviceId, queueMessage.time.ToString()));

            // Compare uuid to find which message will be processed
            if (!queueMessage.uuid.Equals(sentDataFrame?.uuid))
            {
                return;
            }

            // Check valid of device and send to processing queue
            var valid = (await inDeviceInfo.RetrieveEntityAsync(
                new DeviceInfo<int>(queueMessage.deviceId, DeviceInfoSchema.Valid)))?.value;

                if (valid == 1 && !queueMessage.sdId.StartsWith(ExternalConfig.KittingSdId))
                {
                    var queueMsg = new CloudQueueMessage(message);
                    await processingQueue.AddMessageAsync(queueMsg);
                }

            // Delete SentDataFrame record
            try
            {
                await inDataFrame.DeleteEntityAsync(sentDataFrame);
            }
            catch (StorageException se)
            {
                if (se.RequestInformation.HttpStatusCode != 412)
                {
                    throw;
                }
            }
            logger.Debug($"Generate driveData trigger: {message}");
        }
    }
}