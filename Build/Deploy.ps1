Param(
    [parameter(Mandatory = $false)]
    [string] $envFolder = "C:\JB2\DeployEnvironment",

    [parameter(Mandatory = $true)]
    [ValidateScript( {Test-Path $_ })]
    [String] $imageArea,

    [parameter(Mandatory = $true)]
    [string] $subscriptionName,

    [parameter(Mandatory = $true)]
    [string] $resourceGroup,

    [parameter(Mandatory = $true)]
    [string] $userAzure,

    [parameter(Mandatory = $true)]
    [securestring] $passAzure,

    [parameter(Mandatory = $false)]
    [bool] $enableAfterDeploy = $true,

    [parameter(Mandatory = $true, HelpMessage = "Enter one or more function app names separated by commas.")]
    [AllowNull()] 
    [AllowEmptyCollection()] 
    [ValidateSet("api", "driveproc", "mainproc", "healthcheck", "recoveryproc")]
    [string[]] $functionAppItems,
    
    [parameter(Mandatory = $false, HelpMessage = "Enter one or more webapp names separated by commas.")]
    [AllowNull()] 
    [AllowEmptyCollection()]
    [ValidateSet("bom")]
    [string[]] $webAppItems,

    [parameter(Mandatory = $false)]
    [bool] $javacalc = $false,

    [parameter(Mandatory = $false)]
    [bool] $enableWebJob = $true
)

# check if git is installed
if (git --version) {
    Write-Host "[v] Git installed"
}
else {
    Write-Warning "[x] Git does not exist. Please install and try again."
    exit
}

# check if module AzureRM installed
$AzureRMVersion = (Get-Module -ListAvailable -Name AzureRM -Refresh).Version.Major
if ($AzureRMVersion) {
    Write-Host "[v] AzureRM installed."
}
else {
    Write-Warning "[x] Please install using [Install-Module AzureRM] and try again."
    exit
}

# login to Azure
$login = Login-AzureRmAccount -Credential (New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $userAzure, $passAzure)
if ($login) {
    Write-Host "[v] Logged in."
}
else {
    Write-Warning "[x] Login Fail."
    exit
}

# change the subscription
$subscription = Select-AzureRmSubscription -SubscriptionName "$subscriptionName"
if ($subscription) {
    Write-Host "[v] Change the subscription."
}
else {
    Write-Warning "[x] Change the subscription Fail."
    exit
}

# get prefix from tag
$prefix = (Get-AzureRmResourceGroup -Name $resourceGroup).Tags["prefix"]
if ($prefix) {
    Write-Host "[v] Environment Prefix: $prefix"
}
else {
    Write-Warning "[x] Can't get prefix from tags of resource group."
    exit
}

# - Get Path of version Info
$versionPath = Join-Path -Path (Split-Path -Parent $MyInvocation.MyCommand.Path) -ChildPath "VERSION.txt"
$contentVersion = Get-Content -Path $versionPath

# deploy function app
if ($functionAppItems) {
    # Step1: get suffix function for deployment
    $projectList = @("API", "DriveProc", "MainProc", "Healthcheck", "RecoveryProc")
    $functionAppList = @("api", "driveproc", "mainproc", "healthcheck", "recoveryproc")

    for ($projectIndex = 0; $projectIndex -lt $projectList.Count; $projectIndex++) {
        $functionAppName = $functionAppList[$projectIndex]

        if ($functionAppName -in $functionAppItems) {
            [Object[]] $deployFunctionAppList += @{$projectList[$projectIndex] = $functionAppList[$projectIndex]}
        }
    }

    Write-Host "----------------------------------------------------------------"
    Write-Host "[v] DEPLOY FUNCTION APP"
    Write-Output "Deploy Function app list :"
    Write-Output $deployFunctionAppList
    Write-Host "Environment Prefix: $prefix"
    Write-Host "Deploy artifact repo: $envFolder"
    Write-Host "Image Area: $imageArea"
    Write-Host "Subscription Name: $subscriptionName"
    Write-Host "Resource Group: $resourceGroup"
    Write-Host "User:  $userAzure"
    Write-Host "Start function app after deploy: $enableAfterDeploy"

    # deploy each function app
    foreach ($functionAppItem in $deployFunctionAppList) {
        # Step2: get folder path, suffix function 
        $funcAppPath = Join-Path $imageArea ($functionAppItem.Keys)
        $funcAppName = $prefix + '-' + ($functionAppItem.Values)

        # Step3: get Publishing credentials for the WebApp :
        $publicSettings = Invoke-AzureRmResourceAction -ResourceGroupName $resourceGroup -ResourceType Microsoft.Web/sites/config ` -ResourceName "$funcAppName/publishingcredentials" -Action list -ApiVersion 2015-08-01 -Force
        $userFunc = $publicSettings.properties.publishingUserName
        $passFunc = $publicSettings.properties.publishingPassword
        $GIT_URL = "https://$userFunc`:$passFunc@$($funcAppName).scm.azurewebsites.net`:443/$($funcAppName).git"

        # Step4: git clone with master branch
        $GIT_FOLDER_FUNC = "$envFolder\$resourceGroup\$($funcAppName)"
        if (!(Test-Path $GIT_FOLDER_FUNC)) {
            # create resource group folder if not exist
            New-Item -ItemType Directory -Force -Path "$envFolder\$resourceGroup"
            Set-Location "$envFolder\$resourceGroup"
            Write-Host ">>>>>> $funcAppName function app : -git clone"
            git clone $GIT_URL
        }

        # git pull and deploy 
        if ((Test-Path $GIT_FOLDER_FUNC)) {
            # git config user
            Set-Location $GIT_FOLDER_FUNC
            git config http.postBuffer 3M
            git config user.email $userAzure
            git config user.name $userAzure

            # Step5: git pull for HEAD on top master branch
            git pull origin master

            # Step6: delete old file
            Write-Host ">>>>>> $funcAppName function app : -delete old file: $GIT_FOLDER_FUNC\*"
            Get-ChildItem $GIT_FOLDER_FUNC -Recurse | Remove-Item -Force -Recurse

            # Step7: copy new file
            Write-Host ">>>>>> $funcAppName function app : -copy file: $funcAppPath\* -> $GIT_FOLDER_FUNC "
            Copy-Item "$funcAppPath\*" -Destination $GIT_FOLDER_FUNC -Force -Recurse

            # Step8: stop Azure function
            Write-Host ">>>>>>  $funcAppName function app : -stop azure function app."
            Stop-AzureRmWebApp -ResourceGroupName $resourceGroup -Name "$($funcAppName)"

            # Step9: git commit
            Write-Host ">>>>>>  $funcAppName function app : -git commit."
            git add -A
            git commit -m "$funcAppName $($contentVersion)"

            # Step10: git push
            Write-Host ">>>>>>  $funcAppName function app : -git push."
            git push origin master
            Start-Sleep -Seconds 3

            # Step11: start function app
            if ($enableAfterDeploy) {
                Write-Host ">>>>>> $funcAppName function app : -start azure function app."
                Start-AzureRmWebApp -ResourceGroupName $resourceGroup -Name "$($funcAppName)"
            }
        }
        Write-Host "[v] END DEPLOY FUNCTION APP : $funcAppName"
        Write-Host "----------------------------------------------------------------"
    }
}

# deploy webapp
if ($webAppItems) {
    # Step1: get suffix webapp for deployment
    $webAppProjectList = @("BOM")
    $webAppList = @("bom")

    for ($webAppIndex = 0; $webAppIndex -lt $webAppProjectList.Count; $webAppIndex++) {
        $webAppItem = $webAppList[$webAppIndex]

        if ($webAppItem -in $webAppItems) {
            [Object[]] $deployWebAppList += @{$webAppProjectList[$webAppIndex] = $webAppList[$webAppIndex]}
        }
    }
    
    Write-Host "----------------------------------------------------------------"
    Write-Host " DEPLOY WEBAPP"
    Write-Output "Deploy Webapp list :"
    Write-Output $deployWebAppList
    Write-Host "Environment Prefix: $prefix"
    Write-Host "Deploy artifact repo: $envFolder"
    Write-Host "Image Area: $imageArea"
    Write-Host "Subscription Name: $subscriptionName"
    Write-Host "Resource Group: $resourceGroup"
    Write-Host "User:  $userAzure"
    Write-Host "Start WebApp after deploy: $enableAfterDeploy"
    Write-Host "Start WebJob after deploy: $enableWebJob"
    
    # deploy each webapp
    foreach ($deployWebAppItem in $deployWebAppList) {
        # Step2: get folder path, suffix function 
        $webAppPath = Join-Path $imageArea ($deployWebAppItem.Keys)
        $webAppName = $prefix + '-' + ($deployWebAppItem.Values)
        
        # Step3: get Publishing credentials for the WebApp :
        $publishingCredentials = Invoke-AzureRmResourceAction -ResourceGroupName $resourceGroup -ResourceType Microsoft.Web/sites/config ` -ResourceName "$webAppName/publishingcredentials" -Action list -ApiVersion 2015-08-01 -Force
        $userWebApp = $publishingCredentials.properties.publishingUserName
        $passWebApp = $publishingCredentials.properties.publishingPassword
        
        # Step4: git Clone,Pull
        $GIT_URL_WEBAPP = "https://$userWebApp`:$passWebApp@$($webAppName).scm.azurewebsites.net`:443/$($webAppName).git"
        $GIT_FOLDER_WEBAPP = "$envFolder\$resourceGroup\$webAppName"
    
        if (!(Test-Path $GIT_FOLDER_WEBAPP)) {
            # create resource group folder if not exist
            New-Item -ItemType Directory -Force -Path "$envFolder\$resourceGroup"
            Set-Location "$envFolder\$resourceGroup"
            Write-Host ">>>>>> $webAppName webapp : -git clone"
            
            git clone $GIT_URL_WEBAPP
        }
    
        if (Test-Path $GIT_FOLDER_WEBAPP) {
            # git config user
            Set-Location $GIT_FOLDER_WEBAPP
            git config http.postBuffer 3M
            git config user.email $userAzure
            git config user.name $userAzure

            # Step5: git pull for HEAD on top master branch
            Write-Host ">>>>>> $webAppName webapp : -git pull"
            Set-Location $GIT_FOLDER_WEBAPP
            git pull origin master
            
            # Step6: remove old file
            Write-Host ">>>>>> $webAppName webapp : -delete old file: $GIT_FOLDER_WEBAPP"
            Get-ChildItem $GIT_FOLDER_WEBAPP -Recurse | Remove-Item -Force -Recurse

            # Step7: copy file archive file to folder Git
            Write-Host ">>>>>> $webAppName webapp : -copy file: $webAppPath\* -> $GIT_FOLDER_WEBAPP"
            Copy-Item $webAppPath\* -Destination $GIT_FOLDER_WEBAPP -Force -Recurse
            
            # Step8: stop webjob
            $webJobPath = Join-Path $webAppPath "App_Data\jobs"
            if (Test-Path $webJobPath) {
                $webJobTypes = (Get-ChildItem -Path $webJobPath -Depth 0 -Directory -Recurse -Force).Name
                foreach ($webJobType in $webJobTypes) {
                    $webJobTypePath = Join-Path $webJobPath $webJobType
                    $webJobNames = (Get-ChildItem -Path $webJobTypePath -Depth 0 -Directory -Recurse -Force).Name
                    foreach ($webJobName in $webJobNames) {
                        try {
                            Write-Host ">>>>>> $webJobName webjob : -stop webjob"
                            Invoke-AzureRmResourceAction -ApiVersion "2015-08-01" -ResourceGroupName $resourceGroup -ResourceName $webAppName -ResourceType "microsoft.web/sites/$webJobType`webjobs/$webJobName" -Action stop -Force
                        }
                        catch {
                            Write-Host ">>>>>> $webJobName webjob :  -can't stop webjob"
                        }
                    }
                }
            }

            # Step9: Stop webapp
            try {
                Write-Host ">>>>>> $webAppName webapp : -stop webapp"
                Stop-AzureRmWebApp -ResourceGroupName $resourceGroup -Name $webAppName
            }
            catch {
                Write-Host ">>>>>> $webAppName webapp :  -can't start webapp"
            }

            # Step10: commit git 
            # git commit
            Set-Location $GIT_FOLDER_WEBAPP
            git add -A
            Write-Host ">>>>>> $webAppName webapp :  -git commit"
            git commit -m "$contentVersion "
            
            # git push
            Write-Host ">>>>>> $webAppName webapp :  -git push"
            git push origin master

            # Step11: start webjob
            if ($enableWebJob -and (Test-Path $webJobPath)) {
                $webJobTypes = (Get-ChildItem -Path $webJobPath -Depth 0 -Directory -Recurse -Force).Name
                foreach ($webJobType in $webJobTypes) {
                    $webJobTypePath = Join-Path $webJobPath $webJobType
                    $webJobNames = (Get-ChildItem -Path $webJobTypePath -Depth 0 -Directory -Recurse -Force).Name
                    foreach ($webJobName in $webJobNames) {
                        try {
                            Write-Host ">>>>>> $webJobName webjob : -start webjob"
                            Invoke-AzureRmResourceAction -ApiVersion "2015-08-01" -ResourceGroupName $resourceGroup -ResourceName $webAppName -ResourceType "microsoft.web/sites/$webJobType`webjobs/$webJobName" -Action start -Force
                        }
                        catch {
                            Write-Host ">>>>>> $webJobName webjob :  -can't start webjob"
                        }
                    }
                }
            }

            # Step12: start webapp
            if ($enableAfterDeploy) {
                try {
                    Write-Host ">>>>>> $webAppName webapp : -start webapp"
                    Start-AzureRmWebApp -ResourceGroupName $resourceGroup -Name $webAppName
                }
                catch {
                    Write-Host ">>>>>> $webAppName webapp :  -can't start webapp"
                }
            }
        }
        Write-Host "[v] END DEPLOY WEBAPP"
        Write-Host "----------------------------------------------------------------"
    }
}

# deploy javacalc
if ($javacalc) {
    Write-Host "----------------------------------------------------------------"
    Write-Host " DEPLOY JavaCalc"
    Write-Host "Environment Prefix: $prefix"
    Write-Host "Deploy artifact repo: $envFolder"
    Write-Host "Image Area: $imageArea"
    Write-Host "Subscription Name: $subscriptionName"
    Write-Host "Resource Group: $resourceGroup"
    Write-Host "User:  $userAzure"
    Write-Host "Start WebApp after deploy: $enableAfterDeploy"

    # Step1: get folder path, suffix function 
    $webAppName = $prefix + '-' + "javacalc"
    $webAppWarPath = Join-Path $imageArea "JavaCalc\webapps\ROOT.war"

    # Step2: get Publishing credentials for the WebApp :
    $publishingCredentials = Invoke-AzureRmResourceAction -ResourceGroupName $resourceGroup -ResourceType Microsoft.Web/sites/config ` -ResourceName "$webAppName/publishingcredentials" -Action list -ApiVersion 2015-08-01 -Force

    # Step3: stop webapp
    try {
        Write-Host ">>>>>> $webAppName webapp : -stop webapp"
        Stop-AzureRmWebApp -ResourceGroupName $resourceGroup -Name $webAppName
    }
    catch {
        Write-Host ">>>>>> $webAppName webapp : -can't stop webapp"
    }

    # Step4: deploy war
    $base64AuthInfo = [Convert]::ToBase64String([Text.Encoding]::ASCII.GetBytes(("{0}:{1}" -f $publishingCredentials.properties.publishingUserName, $publishingCredentials.properties.publishingPassword)))
    $apiUrl = "https://$webAppName.scm.azurewebsites.net/api/wardeploy"
    try {
        Write-Host ">>>>>> $webAppName webapp : -deploying $webAppWarPath "
        Invoke-RestMethod -Uri $apiUrl -Headers @{Authorization = ("Basic {0}" -f $base64AuthInfo)} -Method POST -InFile $webAppWarPath -ContentType "multipart/form-data"
        Write-Host ">>>>>> $webAppName webapp : -deployed successfully"
    }
    catch {
        Write-Host ">>>>>> $webAppName webapp : -can't deploy war. Exception: $_.Exception.Message"
    }

    # Step5: start webapp
    if ($enableAfterDeploy) {
        try {
            Write-Host ">>>>>> $webAppName webapp : -start webapp"
            Start-AzureRmWebApp -ResourceGroupName $resourceGroup -Name $webAppName
        }
        catch {
            Write-Host ">>>>>> $webAppName webapp : -can't start webapp"
        }
    }

    Write-Host "[v] END DEPLOY JavaCalc"
    Write-Host "----------------------------------------------------------------"
}