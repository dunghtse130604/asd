Param(
    [parameter(Mandatory = $true)]
    [ValidateScript( {Test-Path $_ })]
    [string] $imageArea,

    [parameter(Mandatory = $true)]
    [string] $archiveUser,

    [parameter(Mandatory = $true)]
    [securestring] $archivePass,

    [parameter(Mandatory = $false)]
    [string] $archivePath = "C:\JB2\ArchiveGW\JB2.Archive",

    [parameter(Mandatory = $false)]
    [system.uri] $remoteURL = "https://git3.fsoft.com.vn/fsoft/JB2.Archive.git",

    [parameter(Mandatory = $true)]
    [ValidateSet("bom", "master")]
    [string] $branch
)

Write-Host "--------------------------------"
Write-Host "START ARCHIVE SCRIPT"
Write-Host "Image Area: $imageArea"
Write-Host "Archive User: $archiveUser"
Write-Host "Archive Path: $archivePath"
Write-Host "Remote URL: $remoteURL"
Write-Host "Branch: $branch"

#Step1: Get tagVersion
$versionContent = Get-Content -Path "$imageArea\Script\VERSION.txt"
$version, $revisionHash, $date, $time, $author= $versionContent.Split(' ')
Write-Host "Tag Version: $version"
Write-Host "Commit Build: $revisionHash"
Write-Host "Date: $date"
Write-Host "Time: $time"
Write-Host "Author: $author"

if ($version) {
    Add-Type -AssemblyName System.Web
    $BSTR = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($archivePass)
    $plainPassword = [System.Runtime.InteropServices.Marshal]::PtrToStringAuto($BSTR)
    $gitCredential = ("{0}:{1}@" -f $archiveUser, [System.Web.HttpUtility]::UrlEncode($plainPassword))
    $urlCredential = $remoteURL.AbsoluteUri.Insert($remoteURL.AbsoluteUri.IndexOf($remoteURL.Host), $gitCredential)

    #Step2: Checkout remote
    if (!(Test-Path $archivePath)) {
        New-Item -ItemType Directory -Force -Path $archivePath
        Set-Location $archivePath
        git init
        git remote add origin $urlCredential
        git config http.postBuffer 524288
        git config user.email $archiveUser
        git config user.name $archiveUser
    }

    Set-Location $archivePath
    # Update remote URL to local
    git remote set-url origin $urlCredential

    # Update and switch branch in the Git repository
    git checkout -b $branch
    git pull origin $branch

    # Delete all local tags and get the list of remote tags
    git tag -l | foreach { git tag -d $_ }
    git fetch origin $branch

    # Force the local branch to remote branch
    git reset --hard "origin/$branch"

    #Step3: Remove all file in Git repository
    Write-Host ">>>>>> Archive : remove old file $archivePath\*"
    Get-ChildItem $archivePath -Recurse | Remove-Item -recurse -force

    #Step4: Copy file from build area to Git repository
    if($branch -eq "bom"){
        Copy-Item -Path "$imageArea\BOM" -Destination $archivePath -recurse -force
        Copy-Item -Path "$imageArea\Script" -Destination $archivePath -recurse -force
    }

    if($branch -eq "master"){
        Copy-Item -Path "$imageArea\*" -Destination $archivePath -recurse -force -Exclude @("BOM")
    }

    #Step5: Push,commit build to Archive repository

    #Stage file
    Write-Host ">>>>>> Archive : git add -A"
    Set-Location "$archivePath"
    git add -A

    #commit message
    Write-Host ">>>>>> Archive : git commit"
    git commit -m "Version: ${version} - Commit ${revisionHash}"

    #Add local tag
    Write-Host ">>>>>> Archive : version $version"
    git tag $version -f

    #push file
    Write-Host ">>>>>> Archive : git push"
    git push --set-upstream $urlCredential $branch

    #push tag
    Write-Host ">>>>>> Archive : git push tag"
    git push --force $urlCredential $version
}
else {
    Write-Warning "[x] Archive script can't get version of VERSION.txt file"
}

Write-Host "END ARCHIVE SCRIPT"
Write-Host "--------------------------------"